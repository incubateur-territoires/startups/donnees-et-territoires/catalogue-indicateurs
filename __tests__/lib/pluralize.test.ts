import { pluralize } from "@/lib/pluralize";

describe("pluralize", () => {
  it("should return a pluralized label for a count of 0", () => {
    expect(
      pluralize("Quartier(s) prioritaire(s) de la ville (QPV)", 0),
    ).toEqual("Quartiers prioritaires de la ville (QPV)");
  });

  it("should return a pluralized label for counts above 1", () => {
    expect(pluralize("Ville(s) lauréate(s) RCV", 3)).toEqual(
      "Villes lauréates RCV",
    );
  });

  it("should return a singular label for a count of 1", () => {
    expect(pluralize("Chef(s) de projet identifié(s)", 1)).toEqual(
      "Chef de projet identifié",
    );
  });

  it("should handle the 'territorial(aux)' term in plural form", () => {
    expect(
      pluralize("Volontaire(s) territorial(aux) en administration", 2),
    ).toEqual("Volontaires territoriaux en administration");
  });

  it("should handle the 'territorial(aux)' term in singular form", () => {
    expect(
      pluralize("Volontaire(s) territorial(aux) en administration", 1),
    ).toEqual("Volontaire territorial en administration");
  });

  it("should handle the 'du(des)' term in plural form", () => {
    expect(pluralize("Nom(s) du(des) porteur(s)", 4)).toEqual(
      "Noms des porteurs",
    );
  });

  it("should handle the 'du(des)' in singular form", () => {
    expect(pluralize("Nom(s) du(des) porteur(s)", 1)).toEqual("Nom du porteur");
  });

  it("should handle the 'de la(des)' in plural form", () => {
    expect(pluralize("Nom(s) de la(des) porteuse(s)", 4)).toEqual(
      "Noms des porteuses",
    );
  });

  it("should handle the 'de la(des)' in singular form", () => {
    expect(pluralize("Nom(s) de la(des) porteuse(s)", 1)).toEqual(
      "Nom de la porteuse",
    );
  });

  it("should handle falsy labels", () => {
    expect(pluralize(null, 1)).toEqual(undefined);
  });
});
