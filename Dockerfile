FROM node:20 as install
WORKDIR /app
COPY package.json package-lock.json /app/
RUN npm ci --only=production --cache /.npm

FROM node:20 as build
ARG NEXT_PUBLIC_AUTH_ENABLED
ENV NEXT_PUBLIC_AUTH_ENABLED=$AUTH_ENABLED
WORKDIR /app
COPY . .
COPY --from=install /.npm /.npm
RUN npm ci --cache /.npm
RUN npm run build

FROM node:20
WORKDIR /app
COPY --from=install /app/node_modules /app/node_modules
COPY --from=build /app/.next /app/.next
COPY --from=build /app/public /app/public

EXPOSE 3000
ENV HOST=0.0.0.0
ENV PORT=3000
ENV NODE_ENV=production

CMD ["node_modules/.bin/next", "start"]
