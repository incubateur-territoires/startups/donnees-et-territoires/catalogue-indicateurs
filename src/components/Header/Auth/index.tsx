"use client";

import React, { Suspense } from "react";

import { HeaderQuickAccessItem } from "@codegouvfr/react-dsfr/Header";
import { useSession, signOut } from "next-auth/react";

import featureRouter from "@/lib/features/router";

export default function HeaderAuth({ id }: { id: string }) {
  const authEnabled = featureRouter.auth();
  const { data: session } = useSession();

  if (!authEnabled) {
    return [];
  }

  return [
    <Suspense key="HeaderAuth">
      {session ? (
        <HeaderQuickAccessItem
          id={`logout-${id}`}
          quickAccessItem={{
            text: "Se déconnecter",
            iconId: "ri-logout-box-line",
            buttonProps: {
              onClick: () => signOut(),
            },
          }}
        />
      ) : (
        <HeaderQuickAccessItem
          id={`login-${id}`}
          quickAccessItem={{
            text: "Se connecter",
            iconId: "fr-icon-account-fill",
            linkProps: {
              href: "/login",
            },
          }}
        />
      )}
    </Suspense>,
  ];
}
