"use client";

import { Badge } from "@codegouvfr/react-dsfr/Badge";
import { Header as HeaderDSFR } from "@codegouvfr/react-dsfr/Header";
import { useQuery } from "@tanstack/react-query";
import { usePathname, useSearchParams } from "next/navigation";

import { fetchDefinitions } from "@/lib/api/indicateurs";
import {
  fetchProgrammes,
  getProgrammesOptions,
  ProgrammeOptions,
} from "@/lib/api/programmes";

import HeaderAuth from "./Auth";

function indicateursMenu(
  status: "pending" | "error" | "success",
  error: Error | null,
  programmes: ProgrammeOptions[] | undefined,
  territoires: string,
  pathname: string,
) {
  switch (status) {
    case "pending":
      return {
        leader: {
          title: "Chargement des indicateurs",
          paragraph: "Veuillez patienter",
        },
        categories: [],
      };
    case "error":
      return {
        leader: {
          title: "Erreur de chargement des indicateurs",
          paragraph: error?.message,
        },
        categories: [],
      };
    case "success":
      if (!programmes) {
        return {
          leader: {
            title: "Aucun indicateur disponible",
            paragraph: "Veuillez réessayer plus tard.",
          },
          categories: [],
        };
      }
      return {
        categories: programmes.map((programme: ProgrammeOptions) => ({
          categoryMainText: programme.label,
          links: programme.options.map((option) => ({
            text: option.label,
            linkProps: {
              href: `/indicateurs/${option.value}?territoires=${territoires}`,
            },
            isActive: pathname.includes(option.value),
          })),
        })),
      };
  }
}

export default function Header() {
  const {
    status,
    error,
    data: programmes,
  } = useQuery({
    queryKey: ["programmes"],
    queryFn: getProgrammes,
  });
  const searchParams = useSearchParams();
  const territoires = searchParams.get("territoires");

  const pathName = usePathname();

  return (
    <HeaderDSFR
      brandTop={
        <>
          RÉPUBLIQUE
          <br />
          FRANÇAISE
        </>
      }
      homeLinkProps={{
        href: "/",
        title: "Accueil – Catalogue d’indicateurs ANCT",
      }}
      operatorLogo={{
        imgUrl: "/logos/logo_ANCT.svg",
        alt: "Logo ANCT",
        orientation: "horizontal",
      }}
      serviceTitle={
        <>
          Catalogue d’indicateurs{" "}
          <Badge as="span" noIcon severity="success">
            Beta
          </Badge>
        </>
      }
      quickAccessItems={[
        {
          iconId: "fr-icon-mail-fill",
          linkProps: {
            href: "mailto:donnees@anct.gouv.fr",
          },
          text: "Contactez-nous",
        },
        ...HeaderAuth({ id: "header" }),
      ]}
      navigation={[
        {
          linkProps: {
            href: "/",
          },
          text: "Accueil",
          isActive:
            !pathName.startsWith("/indicateurs") &&
            !pathName.startsWith("/donnees"),
        },
        {
          text: "Indicateurs",
          megaMenu: indicateursMenu(
            status,
            error,
            programmes,
            territoires ?? "",
            pathName,
          ),
          isActive: pathName.startsWith("/indicateurs"),
        },
        {
          linkProps: {
            href: "/donnees",
          },
          text: "Données",
          isActive: pathName.startsWith("/donnees"),
        },
      ]}
    />
  );
}

export async function getProgrammes(): Promise<ProgrammeOptions[]> {
  const programmes = fetchProgrammes();
  const definitions = await fetchDefinitions();
  return getProgrammesOptions(programmes, definitions);
}
