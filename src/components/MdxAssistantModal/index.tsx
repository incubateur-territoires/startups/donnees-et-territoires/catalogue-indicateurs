"use client";

import React from "react";

import Accordion from "@codegouvfr/react-dsfr/Accordion";
import { createModal } from "@codegouvfr/react-dsfr/Modal";

import { DefinitionIndicateur, Metadata } from "@/types/indicateur";

import MdxAssistant from "../MdxAssistant";
import YamlAssistant from "../YamlAssistant";

type MdxAssistantModalProps = {
  modal: ReturnType<typeof createModal>;
  definition?: Metadata;
  data: DefinitionIndicateur | undefined;
};

const MdxAssistantModal = ({
  modal,
  definition,
  data,
}: MdxAssistantModalProps) => {
  if (!definition) {
    return null;
  }

  return (
    <modal.Component
      title={`Assistants d'ajout d'indicateur dans une fiche territoriale`}
      size="large"
    >
      <div className="fr-mb-4w">
        {data ? (
          <>
            <p>
              Ces assistants sont là pour vous aider en générant les morceaux de
              code à utiliser
            </p>
            <Accordion
              style={{ marginTop: "1rem" }}
              label="Générer les définitions à ajouter aux fichiers de configuration YML"
            >
              <YamlAssistant definition={definition} data={data} />
            </Accordion>
            <Accordion label="Générer le bout de code à ajouter à votre modèle de fiche territoriale">
              <p>
                Cet assistant vous aide à créer un morceau de code à ajouter
                dans le fichier
                <code>.mdx</code> d’un modèle de fiche territoriale.
              </p>
              <MdxAssistant definition={definition} data={data} />
            </Accordion>
          </>
        ) : (
          <div>
            Sélectionnez au moins un territoire pour afficher l&apos;assistant
          </div>
        )}
      </div>
    </modal.Component>
  );
};

export default MdxAssistantModal;
