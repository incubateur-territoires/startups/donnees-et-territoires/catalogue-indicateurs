import React from "react";

import Link from "next/link";

import { Organisation } from "@/types/schema";

type ProducerProps = {
  producer: Organisation;
  prefix?: boolean;
};

const Producer = ({ producer, prefix = true }: ProducerProps) => {
  return (
    <div className="fr-mb-2w">
      {prefix && <>Par </>}
      {producer.url ? (
        <Link
          href={producer.url}
          target="_blank"
          rel="noreferrer"
          className="fr-link"
        >
          {producer.name}
        </Link>
      ) : (
        producer.name
      )}
    </div>
  );
};

export default Producer;
