"use client";

import React from "react";

import { createModal } from "@codegouvfr/react-dsfr/Modal";

import { Metadata } from "@/types/indicateur";

import styles from "./styles.module.scss";
import Computation from "../Computation";

type ComputationModalProps = {
  modal: ReturnType<typeof createModal>;
  definition?: Metadata;
};

const ComputationModal = ({ modal, definition }: ComputationModalProps) => {
  if (!definition) {
    return null;
  }

  return (
    <modal.Component
      className={styles.modal}
      title={`Calcul de l’indicateur`}
      size="large"
    >
      <Computation definition={definition} />
    </modal.Component>
  );
};

export default ComputationModal;
