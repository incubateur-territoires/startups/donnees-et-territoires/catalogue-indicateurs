import React from "react";

import beautify from "js-beautify";
import dynamic from "next/dynamic";

import { Metadata } from "@/types/indicateur";

const SyntaxHighlighter = dynamic(
  () => import("@/components/SyntaxHighlighter"),
  {
    ssr: false,
  },
);

type SourceProps = {
  definition: Metadata;
};

const Computation = ({ definition }: SourceProps) => {
  const formattedSchema = beautify(JSON.stringify(definition.schema));

  return (
    <>
      <div className="fr-mb-4w fr-pb-4w">
        <div className="fr-text--lead">Formule de calcul SQL</div>
        <SyntaxHighlighter language="sql">
          {definition.recette.code}
        </SyntaxHighlighter>
      </div>
      <div className="fr-mb-4w">
        <div className="fr-text--lead">Format des données</div>
        <SyntaxHighlighter language="json">{formattedSchema}</SyntaxHighlighter>
      </div>
    </>
  );
};

export default Computation;
