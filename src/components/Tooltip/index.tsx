import MuiTooltip, { TooltipProps } from "@mui/material/Tooltip";
import clsx from "clsx";

import styles from "./styles.module.scss";

export default function Tooltip({ className, ...props }: TooltipProps) {
  return (
    <MuiTooltip
      {...props}
      role="tooltip"
      classes={{
        tooltip: clsx(styles.tooltip, className),
        arrow: styles.arrow,
      }}
    />
  );
}
