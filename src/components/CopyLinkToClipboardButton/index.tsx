"use client";

import React from "react";

import Link from "next/link";
import { useSearchParams } from "next/navigation";

import { useSuccessTimer } from "@/hooks/useSuccessTimer";
import { copyLinkToClipboard } from "@/lib/copy/copyLinkToClipboard";

import Tooltip from "../Tooltip";

type CopyLinkToClipboardButtonProps = {
  searchTerm: string;
};

const CopyLinkToClipboardButton = ({
  searchTerm,
}: CopyLinkToClipboardButtonProps) => {
  const [showSuccess, setShowSuccess] = useSuccessTimer();

  const searchParams = useSearchParams();

  return (
    <Tooltip
      open={showSuccess}
      title="Copié"
      arrow
      // Disable regular hover / focus opening
      onOpen={() => {}}
      onClose={() => {}}
      placement="right"
    >
      <div>
        <Link
          className="fr-link fr-text--sm fr-link--icon-right ri-links-line"
          href="#"
          onClick={() => {
            copyLinkToClipboard(searchTerm, searchParams);

            if (!showSuccess) {
              setShowSuccess(true);
            }
          }}
        >
          Partager cette page
        </Link>
      </div>
    </Tooltip>
  );
};

export default CopyLinkToClipboardButton;
