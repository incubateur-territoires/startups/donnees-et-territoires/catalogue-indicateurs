import React from "react";

import { formatDate } from "@/lib/formatDate";
import { formatPeriodicity } from "@/lib/formatPeriodicity";
import { Metadata } from "@/types/indicateur";

import Maintainer from "../indicateurs/Maintainer";

type SourceProps = {
  definition: Metadata;
};

export const Source = ({ definition }: SourceProps) => {
  const hasDatasets = definition.datasets.length > 0;

  return hasDatasets ? (
    definition.datasets.map((dataset) => {
      const periodicity = formatPeriodicity(dataset.updateFrequency);

      return (
        <div className="fr-my-2w" key={dataset.table}>
          <div className="fr-text--bold">{dataset.name}</div>
          <ul>
            <li>Source : {dataset.producer?.name}</li>
            <li>Nom du fichier importé : {dataset.pathPattern}</li>
            <li>Format du fichier : {dataset.fileFormat}</li>
            <li>Périodicité : {periodicity}</li>
            <li>
              Responsable : <Maintainer maintainer={dataset.maintainer ?? ""} />
            </li>
            <li>Date de mise à jour : {formatDate(dataset.lastModified)}</li>
          </ul>
        </div>
      );
    })
  ) : (
    <div className="fr-my-2w">
      Les jeux de données ne sont pas disponibles pour cet indicateur.
    </div>
  );
};

export default Source;
