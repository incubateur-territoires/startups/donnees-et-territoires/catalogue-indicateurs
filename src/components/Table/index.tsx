import clsx from "clsx";

import { formatNumericValue } from "@/lib/formatNumericValue";

import styles from "./styles.module.scss";

type TableProps<T> = {
  headers: string[];
  data: T[][];
  className?: string;
  renderCell?: (item: T, index: number) => React.ReactNode;
};

export default function Table<T>({
  headers,
  data,
  className,
  renderCell: renderCellProp,
}: TableProps<T>) {
  function defaultRenderCell(item: T, index: number) {
    const value =
      typeof item === "number" || typeof item === "string"
        ? formatNumericValue({ value: item })
        : "-";

    return (
      <td className={styles.cell} key={index}>
        {value}
      </td>
    );
  }

  const renderCell = renderCellProp || defaultRenderCell;
  return (
    <div className={clsx("fr-table", className)}>
      <table>
        <thead style={{ backgroundSize: "100% 2px" }}>
          <tr>
            {headers.map((header) => (
              <th key={header}>{header}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {data.map((line, index) => (
            <tr key={index}>{line.map(renderCell)}</tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
