import { useEffect, useState } from "react";

import Button from "@codegouvfr/react-dsfr/Button";
import { RadioButtons } from "@codegouvfr/react-dsfr/RadioButtons";
import clsx from "clsx";
import { Controller, useForm } from "react-hook-form";

import { writeToClipboard } from "@/lib/copy/writeToClipboard";
import {
  getAvailableIndicateurDescriptorOptions,
  indicateurDescriptors,
  IndicateurDescriptorType,
} from "@/lib/getIndicateurDescriptor";
import { DefinitionIndicateur, Metadata } from "@/types/indicateur";

import SyntaxHighlighter from "../SyntaxHighlighter";

export type PreviewFormSchema = {
  indicateurType: { value: IndicateurDescriptorType; label: string };
  label: string;
  labelListe?: string;
};

type SourceProps = {
  definition: Metadata;
  data: DefinitionIndicateur | undefined;
};

const MdxAssistant = ({ definition, data }: SourceProps) => {
  const [hasSubmitted, setHasSubmitted] = useState(false);
  const [mdxProposition, setMdxProposition] = useState<string | null>(null);
  const mdxIndicateursTypeOptions = () => {
    if (!data) {
      return [];
    }
    return getAvailableIndicateurDescriptorOptions(data);
  };

  const { handleSubmit, control, watch } = useForm<PreviewFormSchema>({
    defaultValues: {
      indicateurType: {},
      label: definition.nom || "",
      labelListe: "",
    },
  });

  const indicateurTypeValue = watch("indicateurType");
  const labelValue = watch("label");
  const labelListeValue = watch("labelListe");

  useEffect(() => {
    setHasSubmitted(false);
  }, [indicateurTypeValue, labelValue, labelListeValue]);

  const onSubmit = async (data: PreviewFormSchema) => {
    setHasSubmitted(true);

    const indicateurDescriptor =
      indicateurDescriptors[data.indicateurType.value];
    const id = `${indicateurDescriptor.key_id}="${definition.identifiant}"`;
    const label = `${indicateurDescriptor.key_label}="${data.label}"`;
    const labelListe =
      data.labelListe && indicateurDescriptor.key_label_liste
        ? `${indicateurDescriptor.key_label_liste}="${data.labelListe}"`
        : "";

    if (data.indicateurType.value === IndicateurDescriptorType.TABLEAU) {
      const definitionColonnes = definition.schema.items?.properties;
      const createMdxColonne = ({
        name,
        title,
      }: {
        name: string;
        title: string;
      }) => {
        return `<Colonne nom="${name}" titre="${title}" />`;
      };
      const getColonneNameAndTitle = (nbColonne: number) => {
        const colprops =
          definitionColonnes && Object.entries(definitionColonnes)[nbColonne];
        const key = colprops && colprops[0];
        const value = colprops && colprops[1];
        const name = key || "id de la colonne";
        const title = value?.title ? value.title : key || "titre de la colonne";
        return { name, title };
      };
      const colonne1 = createMdxColonne(getColonneNameAndTitle(0));
      const colonne2 = createMdxColonne(getColonneNameAndTitle(1));

      setMdxProposition(
        `<${data.indicateurType.value} ${id} ${label}>
  ${colonne1}
  ${colonne2}
</${data.indicateurType.value}>`,
      );
    } else {
      setMdxProposition(
        `<${data.indicateurType.value} ${id} ${label} ${labelListe} />`,
      );
    }
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Controller
        name="label"
        control={control}
        render={({ field: { onChange, onBlur, value } }) => (
          <>
            <label htmlFor="label" className={clsx("fr-label")}>
              Libellé
              <span className="fr-hint-text">
                Ecrire la version plurielle entre parenthèses. Par exemple : «
                <i>Ville(s) ACV</i> ».
              </span>
            </label>
            <input
              className={clsx("fr-input", "fr-mb-3w")}
              placeholder="Ex: Ville(s) ACV"
              onChange={onChange}
              onBlur={onBlur}
              value={value}
              type="text"
            />
          </>
        )}
      />
      <Controller
        name="indicateurType"
        control={control}
        render={({ field: { onChange, onBlur, value } }) => (
          <RadioButtons
            id={`indicateurType-${definition.identifiant}`}
            legend="Type d’affichage de l’indicateur"
            options={mdxIndicateursTypeOptions().map((option) => ({
              label: option.label,
              value: option.value,
              nativeInputProps: {
                checked: option.value === value?.value,
                onChange: () => onChange(option),
                onBlur,
              },
            }))}
          />
        )}
      />
      {IndicateurDescriptorType.INDICATEUR_LISTE ===
        indicateurTypeValue?.value && (
        <Controller
          name="labelListe"
          control={control}
          render={({ field: { onChange, onBlur, value } }) => (
            <>
              <label htmlFor="labelListe" className={clsx("fr-label")}>
                Libellé de la liste détaillée
                <span className="fr-hint-text">
                  Vous pouvez écrire la version au pluriel entre parenthèses.
                  Par exemple : « <i>Liste des ville(s) ACV</i> ».
                </span>
              </label>
              <input
                className={clsx("fr-input", "fr-mb-3w")}
                placeholder="Ex: Liste des ville(s) ACV"
                onChange={onChange}
                onBlur={onBlur}
                value={value}
                type="text"
              />
            </>
          )}
        />
      )}
      <Button
        type="submit"
        className={clsx("fr-mb-3w")}
        priority="secondary"
        disabled={!indicateurTypeValue || !labelValue}
      >
        Générer le code MDx
      </Button>
      {hasSubmitted && mdxProposition && (
        <>
          <SyntaxHighlighter language="markdown">
            {mdxProposition}
          </SyntaxHighlighter>
          <div style={{ textAlign: "end" }}>
            <Button
              priority="tertiary"
              iconId="ri-file-copy-line"
              title="Copier le code mdx généré"
              onClick={() => {
                writeToClipboard(mdxProposition, mdxProposition);
              }}
            >
              Copier
            </Button>
          </div>
        </>
      )}
      <p>
        Vous pouvez copier-coller le code généré dans le fichier{" "}
        <code>.mdx</code> des fiches correspondantes. Attention le code généré
        est une proposition, il peut être nécessaire de l&apos;adapter. Pour
        avoir plus d&apos;informations pour adapter et compléter cette
        proposition, consultez{" "}
        <a
          title="le guide utilisateur - ouvre une nouvelle fenêtre"
          href="https://gitlab.donnees.incubateur.anct.gouv.fr/infrastructure/generateur-fiches-anct/-/blob/main/Guide_utilisateur.md?ref_type=heads"
          target="_blank"
          rel="noopener external"
        >
          le guide utilisateur.
        </a>
      </p>
    </form>
  );
};

export default MdxAssistant;
