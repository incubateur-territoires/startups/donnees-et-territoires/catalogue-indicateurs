"use client";

import React, { RefObject } from "react";

import { Button } from "@codegouvfr/react-dsfr/Button";
import { Chart } from "chart.js";

import { useSuccessTimer } from "@/hooks/useSuccessTimer";
import { copyChartToClipboard } from "@/lib/copy/copyChartToClipboard";
import { copyIndicateurToClipboard } from "@/lib/copy/copyIndicateurToClipboard";
import { copyMapToClipboard } from "@/lib/copy/copyMapToClipboard";
import { hasClipboardSupport } from "@/lib/copy/hasClipboardSupport";
import { IndicateurViewData } from "@/lib/indicateurs/extractIndicateurViewData";
import { isSafari } from "@/lib/isSafari";
import { Metadata } from "@/types/indicateur";

import Tooltip from "../Tooltip";

type CopyToClipboardButtonProps = {
  data: IndicateurViewData[];
  definition: Metadata;
  view: string;
  chartRef?: RefObject<Chart<"bar" | "pie", (string | number)[]>>;
  mapRef: RefObject<HTMLDivElement | null>;
  className?: string;
};

const CopyToClipboardButton = ({
  data,
  definition,
  view,
  chartRef,
  mapRef,
  className,
}: CopyToClipboardButtonProps) => {
  const [showSuccess, setShowSuccess] = useSuccessTimer();

  // Firefox does not support copying images to clipboard, even with polyfill
  // so we will just hide the button on this browser for now.
  const canCopyCharts = hasClipboardSupport();
  // Safari cannot copy leaflet maps to clipboard
  const canCopyMaps = hasClipboardSupport() && !isSafari();

  const isChart =
    ["bar", "pie"].includes(view) && definition.dimensions.length === 0;

  const isMap = view === "map";

  return (
    <Tooltip
      open={showSuccess}
      title="Copié"
      arrow
      // Disable regular hover / focus opening
      onOpen={() => {}}
      onClose={() => {}}
      placement="right"
    >
      <div className={className}>
        <Button
          iconId="ri-file-copy-line"
          size="small"
          onClick={(event: React.MouseEvent) => {
            event.preventDefault();

            if (!showSuccess) {
              setShowSuccess(true);
            }

            if (isChart && canCopyCharts && chartRef) {
              copyChartToClipboard(chartRef);

              return;
            }

            if (isMap && canCopyMaps && mapRef) {
              copyMapToClipboard(mapRef);

              return;
            }

            copyIndicateurToClipboard(data, definition);
          }}
          priority="secondary"
        >
          Copier
        </Button>
      </div>
    </Tooltip>
  );
};

export default CopyToClipboardButton;
