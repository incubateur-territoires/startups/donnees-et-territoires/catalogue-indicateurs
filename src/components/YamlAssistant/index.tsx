import { useState } from "react";

import Button from "@codegouvfr/react-dsfr/Button";
import ToggleSwitch from "@codegouvfr/react-dsfr/ToggleSwitch";
import dynamic from "next/dynamic";

import { PROGRAMME_TAG_NAME } from "@/constants/indicateurs";
import { writeToClipboard } from "@/lib/copy/writeToClipboard";
import { IndicateurDescriptorType } from "@/lib/getIndicateurDescriptor";
import { DefinitionIndicateur, Metadata } from "@/types/indicateur";

import styles from "./styles.module.scss";

const SyntaxHighlighter = dynamic(
  () => import("@/components/SyntaxHighlighter"),
  {
    ssr: false,
  },
);

export type PreviewFormSchema = {
  indicateurType: { value: IndicateurDescriptorType; label: string };
  label: string;
  labelListe?: string;
};

type SourceProps = {
  definition: Metadata;
  data: DefinitionIndicateur | undefined;
};

function getDataset(datasetId: string) {
  return `- path : "https://fichiers.donnees.incubateur.anct.gouv.fr/data-packages/${datasetId}/datapackage.json"
  name: "${datasetId}"
  resources:
      - name: "${datasetId}.csv"`;
}

const YamlAssistant = ({ definition, data }: SourceProps) => {
  const [datasetsFileExists, setDatasetsFileExists] = useState(false);
  const [indicateurFileExists, setIndicateurFileExists] = useState(false);
  const [showDatasetCode, setShowDatasetCode] = useState(false);
  const [showIndicateurCode, setShowIndicateurCode] = useState(false);

  const datasetList = data?.metadata?.datasets?.map((d) => getDataset(d.table));
  const datasets =
    "" + datasetList?.reduce((acc, dataset) => acc + "\n" + dataset, "");
  const datasetsFile =
    `profile: tabular-data-package-catalog
resources:` + datasets;

  const indicateur = `- identifiant: "${definition.identifiant}"`;
  const indicateurFile = `profile: indicateurs-catalog
resources:
- path: https://servitu.donnees.incubateur.anct.gouv.fr/graphql
  resources:
    - ${indicateur}`;
  const fileName = definition.tags?.find(
    (tag) => tag.name === PROGRAMME_TAG_NAME,
  )?.value;

  const buttonTocopy = (toCopy: string) => {
    return (
      <Button
        priority="primary"
        size="small"
        title="Copier le code mdx généré"
        onClick={() => {
          writeToClipboard(toCopy, toCopy);
        }}
        className={styles["copy"]}
      >
        Copier
      </Button>
    );
  };
  return (
    <div className={styles.indicateur}>
      <div className="fr-h6">1- Générer la définition du jeu de données</div>
      <ToggleSwitch
        inputTitle="the-title"
        label={`Le fichier de configuration du jeu de données ${fileName}.datasets.yml existe-t-il déjà ?`}
        labelPosition="left"
        onChange={() => setDatasetsFileExists(!datasetsFileExists)}
      />
      <Button
        priority="primary"
        onClick={() => setShowDatasetCode(true)}
        className="fr-mb-2w"
      >
        Générer le définition du jeu de données
      </Button>
      {showDatasetCode &&
        (datasetsFileExists ? (
          <>
            <div>
              Compléter le fichier{" "}
              <span className={styles["file-name"]}>
                {fileName}.datasets.yml
              </span>{" "}
              avec le contenu ci-dessous (si nécessaire)
            </div>
            <div className={styles["code-container"]}>
              <SyntaxHighlighter language="yaml">{datasets}</SyntaxHighlighter>
              {buttonTocopy(datasetsFile)}
            </div>
          </>
        ) : (
          <>
            <div>
              Créer le fichier{" "}
              <span className={styles["file-name"]}>
                {fileName}.datasets.yml
              </span>{" "}
              avec le contenu ci-dessous
            </div>
            <div className={styles["code-container"]}>
              <SyntaxHighlighter language="yaml">
                {datasetsFile}
              </SyntaxHighlighter>
              {buttonTocopy(datasetsFile)}
            </div>
          </>
        ))}

      <hr className="fr-my-2w" />
      <div className="fr-h6">2- Générer la définition de l’indicateur</div>
      <ToggleSwitch
        inputTitle="the-title"
        label={`Le fichier de configuration de l'indicateur ${fileName}.indicateurs.yml existe-t-il déjà ?`}
        labelPosition="left"
        onChange={() => setIndicateurFileExists(!indicateurFileExists)}
      />
      <Button
        priority="primary"
        onClick={() => setShowIndicateurCode(true)}
        className="fr-mb-2w"
      >
        Générer le définition de l’indicateur
      </Button>
      {showIndicateurCode &&
        (indicateurFileExists ? (
          <>
            <div>
              Compléter le fichier{" "}
              <span className={styles["file-name"]}>
                {fileName}.indicateurs.yml{" "}
              </span>
              avec le contenu ci-dessous
            </div>
            <div className={styles["code-container"]}>
              <SyntaxHighlighter language="yaml">
                {indicateur}
              </SyntaxHighlighter>
              {buttonTocopy(indicateur)}
            </div>
          </>
        ) : (
          <>
            <div>
              Créer le fichier{" "}
              <span className={styles["file-name"]}>
                {fileName}.indicateurs.yml{" "}
              </span>
              avec le contenu ci-dessous
            </div>
            <div className={styles["code-container"]}>
              <SyntaxHighlighter language="yaml">
                {indicateurFile}
              </SyntaxHighlighter>
              {buttonTocopy(indicateurFile)}
            </div>
          </>
        ))}
    </div>
  );
};

export default YamlAssistant;
