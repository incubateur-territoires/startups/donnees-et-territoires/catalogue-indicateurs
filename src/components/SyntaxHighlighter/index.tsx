"use client";

import { PrismLight as BaseSyntaxHighlighter } from "react-syntax-highlighter";
import json from "react-syntax-highlighter/dist/esm/languages/prism/json";
import markdown from "react-syntax-highlighter/dist/esm/languages/prism/markdown";
import sql from "react-syntax-highlighter/dist/esm/languages/prism/sql";
import yaml from "react-syntax-highlighter/dist/esm/languages/prism/yaml";
import prism from "react-syntax-highlighter/dist/esm/styles/prism/prism";

BaseSyntaxHighlighter.registerLanguage("sql", sql);
BaseSyntaxHighlighter.registerLanguage("json", json);
BaseSyntaxHighlighter.registerLanguage("yaml", yaml);
BaseSyntaxHighlighter.registerLanguage("markdown", markdown);

type SyntaxHighlighterProps = {
  children: string;
  language: "sql" | "json" | "yaml" | "markdown";
};

const SyntaxHighlighter = ({ children, language }: SyntaxHighlighterProps) => {
  return (
    <BaseSyntaxHighlighter language={language} style={prism}>
      {children}
    </BaseSyntaxHighlighter>
  );
};

export default SyntaxHighlighter;
