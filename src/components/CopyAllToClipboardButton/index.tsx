"use client";

import React from "react";

import { UseQueryResult } from "@tanstack/react-query";
import Link from "next/link";

import { useTerritoiresOptionsContext } from "@/context/TerritoiresOptionsContext";
import { useSuccessTimer } from "@/hooks/useSuccessTimer";
import { NetworkError } from "@/lib/api/NetworkError";
import { copyIndicateursToClipboard } from "@/lib/copy/copyIndicateurToClipboard";
import { DefinitionIndicateur, Metadata } from "@/types/indicateur";

import Tooltip from "../Tooltip";

type CopyAllToClipboardButtonProps = {
  definitions: Metadata[];
  data: UseQueryResult<DefinitionIndicateur[], NetworkError>[];
};

const CopyAllToClipboardButton = ({
  definitions,
  data,
}: CopyAllToClipboardButtonProps) => {
  const territoires = useTerritoiresOptionsContext();
  const [showSuccess, setShowSuccess] = useSuccessTimer();

  return (
    <Tooltip
      open={showSuccess}
      title="Copié"
      arrow
      // Disable regular hover / focus opening
      onOpen={() => {}}
      onClose={() => {}}
      placement="right"
    >
      <Link
        className="fr-link fr-text--sm fr-link--icon-right ri-file-copy-line"
        href="#"
        onClick={() => {
          copyIndicateursToClipboard(definitions, data, territoires);

          if (!showSuccess) {
            setShowSuccess(true);
          }
        }}
      >
        Copier les informations de cette page
      </Link>
    </Tooltip>
  );
};

export default CopyAllToClipboardButton;
