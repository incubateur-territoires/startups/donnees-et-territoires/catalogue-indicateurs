export const TABLE_CONFIG = {
  vacance_commerciale: {
    taux: "%",
  },
  logements_vacants: {
    taux: "%",
  },
  bataillons_prevention: {
    dotation_2021: "€",
    dotation_2022: "€",
  },
};

export const TABLE_HEADERS = ["Territoire", "Valeur"];

export const DIMENSIONS_TABLE_HEADERS = ["Territoire", "Total"];
