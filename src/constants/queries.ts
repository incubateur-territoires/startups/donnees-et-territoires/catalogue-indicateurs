export const QUERY_CONFIG = {
  refetchOnMount: false,
  retryOnMount: true,
  refetchOnWindowFocus: false,
  retryDelay: 300,
  staleTime: Infinity,
};
