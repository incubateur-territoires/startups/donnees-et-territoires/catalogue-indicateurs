"use client";

import React, {
  createContext,
  useCallback,
  useContext,
  useMemo,
  useState,
} from "react";

import { createModal } from "@codegouvfr/react-dsfr/Modal";

import ListModal from "@/components/ListModal";
import { Metadata } from "@/types/indicateur";

type ListModalContext = {
  setData: (
    data: string[] | Record<string, unknown>[],
    definition: Metadata,
  ) => void;
  open: () => void;
};

type ListModalProviderProps = {
  children: React.ReactNode;
};

const modal = createModal({
  id: "list-modal",
  isOpenedByDefault: false,
});

const defaultValue = {
  setData: () => {},
  open: () => {},
};

export const ListModalContext = createContext<ListModalContext>(defaultValue);

export const ListModalProvider = ({ children }: ListModalProviderProps) => {
  const [modalData, setModalData] = useState<{
    data: string[] | Record<string, unknown>[];
    definition?: Metadata;
  }>({ data: [], definition: undefined });

  const setData = useCallback(
    (data: string[] | Record<string, unknown>[], definition: Metadata) => {
      setModalData({
        data,
        definition,
      });
    },
    [],
  );

  const open = useCallback(() => {
    modal.open();
  }, []);

  const value = useMemo(
    () => ({
      setData,
      open,
    }),
    [setData, open],
  );

  return (
    <ListModalContext.Provider value={value}>
      <ListModal
        modal={modal}
        data={modalData.data}
        definition={modalData.definition}
      />
      {children}
    </ListModalContext.Provider>
  );
};

export const useListModalContext = (): ListModalContext =>
  useContext(ListModalContext);
