import { Dispatch, SetStateAction, useEffect, useState } from "react";

import { SUCCESS_INDICATOR_DURATION } from "@/constants/copy";

export const useSuccessTimer = (): [
  boolean,
  Dispatch<SetStateAction<boolean>>,
] => {
  const [showSuccess, setShowSuccess] = useState(false);

  useEffect(() => {
    if (showSuccess) {
      const id = setTimeout(
        () => setShowSuccess(false),
        SUCCESS_INDICATOR_DURATION,
      );

      return () => clearTimeout(id);
    }
  }, [showSuccess]);

  return [showSuccess, setShowSuccess];
};
