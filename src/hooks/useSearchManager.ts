import { useMemo, useState } from "react";

import { FUSE_OPTIONS } from "@/constants/search";

import { useFuzzySearch } from "./useFuzzySearch";

export type SearchFormSchema = {
  search: {
    value: string;
    nom: string;
    tags: { name: string; value: string }[];
    label: string;
    originalLabel: string;
  };
};

type SearchEntity = {
  nom: string;
  identifiant: string;
};

export const useSearchManager = <T extends SearchEntity>(definitions: T[]) => {
  const [searchTerm, setSearchTerm] = useState("");

  const results = useFuzzySearch(searchTerm, definitions, FUSE_OPTIONS);

  const options = useMemo(() => {
    const list = searchTerm !== "" ? results : definitions;

    return list.map((definition) => ({
      ...definition,
      label: definition.nom || "",
      value: definition.identifiant,
    }));
  }, [results, searchTerm, definitions]);

  const onInputChange = (input: string) => {
    setSearchTerm(input);
  };

  return { onInputChange, options, searchTerm };
};
