import { DatasetWithIndicateurs } from "./schema";

export enum Source {
  DATA_GOUV = "data.gouv",
  GRIST = "grist",
  AIRTABLE = "airtable",
  MANUAL = "manual",
  METABASE = "metabase",
}

export type DatasetWithProgrammes = DatasetWithIndicateurs & {
  programmes: string[];
};
