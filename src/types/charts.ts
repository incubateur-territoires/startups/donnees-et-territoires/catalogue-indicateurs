import React from "react";

import { UseQueryResult } from "@tanstack/react-query";
import { Chart, ChartTypeRegistry } from "chart.js";

import { DefinitionIndicateur, IndicateurCalcule } from "./indicateur";
import { TerritoireOption } from "./select";

export type ChartTypes = "bar" | "pie";

export type ChartComponentProps<Type extends keyof ChartTypeRegistry> = {
  data: {
    regions: UseQueryResult<IndicateurCalcule>[];
    departements: UseQueryResult<IndicateurCalcule>[];
  };
  selectedValues: {
    regions: TerritoireOption[];
    departements: TerritoireOption[];
  };
  definition: DefinitionIndicateur;
  chartRef: React.RefObject<Chart<Type, (string | number)[]>>;
};
