import React, { useMemo } from "react";

import clsx from "clsx";
import Link from "next/link";
import { useRouter } from "next/navigation";

import Menu from "@/components/Menu";
import { extractQueryTerritoires } from "@/lib/extractQueryTerritoires";
import { generateMetricsListPath } from "@/lib/path/generateMetricsListPath";
import { extractSelectValues } from "@/lib/select/extractSelectValue";
import { Programme } from "@/types/programme";
import { TerritoireOption, TerritoiresOptions } from "@/types/select";

import styles from "../styles.module.scss";

type FiltersProps = {
  territoiresOptions: TerritoiresOptions;
  territoires: string;
  programme?: Programme;
  searchTerm: string;
};

export default function FiltersTerritoires({
  territoiresOptions,
  territoires,
  programme,
  searchTerm,
}: FiltersProps) {
  const router = useRouter();

  const onTerritoireChange = (values: TerritoireOption[]) => {
    const path = generateMetricsListPath({
      programme: programme?.identifiant,
      territoires: values,
      search: searchTerm,
    });

    router.push(path, {
      scroll: false,
    });
  };

  const selectValues = useMemo(() => {
    const selected = extractQueryTerritoires(territoires);

    return extractSelectValues(territoiresOptions, (option) => {
      return selected.some(
        ([maille, code]) =>
          option.value === code && option.maille[0] === maille,
      );
    });
  }, [territoires, territoiresOptions]);

  const territoiresLabel =
    selectValues.length > 0
      ? `${selectValues.length} Territoire${
          selectValues.length !== 1 ? "s" : ""
        } sélectionné${selectValues.length !== 1 ? "s" : ""}`
      : "Régions, départements";

  return (
    <div className={clsx("fr-col-12 fr-col-lg-4 fr-py-0", styles.container)}>
      <div
        className={clsx(
          "fr-mb-1w fr-text--lg",
          styles.weighted,
          styles["white-text"],
        )}
      >
        Sélectionner un ou plusieurs territoires
      </div>
      <Menu
        id="territories"
        label={territoiresLabel}
        options={territoiresOptions}
        value={selectValues}
        onChange={onTerritoireChange}
      />
      {selectValues.length > 0 && (
        <div className={styles["link-container"]}>
          <Link
            className={clsx("fr-link fr-text--sm", styles["white-text"])}
            href={generateMetricsListPath({
              programme: programme?.identifiant,
              search: searchTerm,
            })}
          >
            Supprimer la sélection
          </Link>
        </div>
      )}
    </div>
  );
}
