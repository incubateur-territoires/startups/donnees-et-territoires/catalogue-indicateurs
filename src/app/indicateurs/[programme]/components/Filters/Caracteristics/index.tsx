import React, { ChangeEvent, ChangeEventHandler, useCallback } from "react";

import Button from "@codegouvfr/react-dsfr/Button";
import clsx from "clsx";
import Link from "next/link";

import Checkbox from "@/components/Checkbox";

import styles from "../styles.module.scss";

type FiltersProps = {
  searchTerm: string;
  setSearchTerm: (term: string) => void;
  clearSearch: () => void;
  isCommuneLevel: boolean;
  setIsCommuneLevel: (isCommuneLevel: boolean) => void;
};

export default function FiltersCaracteristics({
  searchTerm,
  setSearchTerm,
  clearSearch,
  isCommuneLevel,
  setIsCommuneLevel,
}: FiltersProps) {
  const onCommuneLevelCheckboxChange: ChangeEventHandler<HTMLInputElement> = (
    event,
  ) => {
    const { checked } = event.target;
    setIsCommuneLevel(checked);
  };

  const handleSearchTermChange = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      setSearchTerm(event.target.value);
    },
    [setSearchTerm],
  );

  return (
    <div className={clsx("fr-col-12 fr-col-lg-4 fr-py-0", styles.container)}>
      <nav
        className="fr-sidemenu fr-sidemenu--right fr-sidemenu--sticky-full-height fr-pl-0"
        role="navigation"
      >
        <div className={clsx("fr-sidemenu__inner", styles.content)}>
          <button
            className="fr-sidemenu__btn"
            hidden
            aria-controls="fr-sidemenu-wrapper"
            aria-expanded="false"
          >
            Filtrer et comparer
          </button>
          <div className={clsx("fr-collapse")} id="fr-sidemenu-wrapper">
            <div>
              <div
                className={clsx(
                  "fr-mb-1w fr-mt-4w fr-text--lg",
                  styles.weighted,
                )}
              >
                Filtrer par mot-clé
              </div>
              <div className={styles["search-container"]}>
                <input
                  className="fr-input"
                  placeholder="Rechercher"
                  value={searchTerm}
                  onChange={handleSearchTermChange}
                />
              </div>
              {searchTerm && (
                <div className={clsx("fr-mt-1w", styles["link-container"])}>
                  <Link
                    className="fr-link fr-text--sm"
                    onClick={clearSearch}
                    href="#"
                  >
                    Supprimer le filtre
                  </Link>
                </div>
              )}
            </div>
            <div>
              <div
                className={clsx(
                  "fr-mb-1w fr-mt-4w fr-text--lg",
                  styles.weighted,
                )}
              >
                Filtrer par maille
              </div>
              <Checkbox
                small
                label="Disponible à la maille commune"
                name="tooltip"
                onChange={onCommuneLevelCheckboxChange}
                checked={isCommuneLevel}
                className="fr-mt-1w"
              />
            </div>
          </div>

          <div className={clsx("fr-mb-2w", styles.cta)}>
            <Button
              linkProps={{
                href: "mailto:donnees@anct.gouv.fr",
              }}
              iconPosition="right"
              iconId="ri-mail-line"
            >
              Signaler une erreur
            </Button>
          </div>
        </div>
      </nav>
    </div>
  );
}
