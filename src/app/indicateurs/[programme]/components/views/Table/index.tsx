import React from "react";

import Button from "@codegouvfr/react-dsfr/Button";
import clsx from "clsx";

import BaseTable from "@/components/Table";
import { DIMENSIONS_TABLE_HEADERS, TABLE_HEADERS } from "@/constants/table";
import { useListModalContext } from "@/context/ListModalContext";
import { formatNumericValue } from "@/lib/formatNumericValue";
import { IndicateurViewData } from "@/lib/indicateurs/extractIndicateurViewData";
import { fillMissingCategories } from "@/lib/indicateurs/fillMissingCategories";
import { getCategories } from "@/lib/indicateurs/getCategories";
import { Metadata } from "@/types/indicateur";
import { Dimension } from "@/types/schema";

import styles from "./styles.module.scss";

type TableProps = {
  data: IndicateurViewData[];
  definition: Metadata;
};

const ViewListCell = ({
  item,
  definition,
}: {
  item: string[] | Record<string, unknown>[];
  definition: Metadata;
}) => {
  const { open, setData } = useListModalContext();

  return (
    <Button
      priority="tertiary no outline"
      size="small"
      iconId="ri-eye-line"
      onClick={() => {
        setData(item, definition);
        open();
      }}
      title="Voir la liste"
    />
  );
};

const Table = ({ data, definition }: TableProps) => {
  const unit = definition.unite;

  const hasList = data?.[0]?.hasList;

  const definitionCategories = getCategories(
    definition.dimensions as Dimension[],
  );

  const headers = [
    ...(definitionCategories ? DIMENSIONS_TABLE_HEADERS : TABLE_HEADERS),
    ...(definitionCategories
      ? definitionCategories.map((category) => category.titre)
      : []),
    ...(hasList ? ["Voir la liste"] : []),
  ];

  return (
    <BaseTable<string | number | string[] | Record<string, unknown>[]>
      headers={headers}
      data={data.map(({ label, count, list, dimensions }) => {
        const rowData: (
          | string
          | number
          | string[]
          | Record<string, unknown>[]
        )[] = [label, formatNumericValue({ value: count, unit })];

        const categories = fillMissingCategories(
          getCategories(dimensions),
          definitionCategories,
        );

        if (hasList && list) {
          rowData.push(list);
        }

        if (categories) {
          for (const category of categories) {
            rowData.push(formatNumericValue({ value: category.valeur, unit }));
          }
        }

        return rowData;
      })}
      renderCell={(item, index) => {
        let value: string | number | React.ReactNode = "-";

        if (typeof item === "string" || typeof item === "number") {
          value = item;
        }

        if (Array.isArray(item)) {
          value = <ViewListCell item={item} definition={definition} />;
        }

        return <td key={`${item}_${index}`}>{value}</td>;
      }}
      className={clsx("fr-p-3v fr-my-0", styles.table)}
    />
  );
};

export default Table;
