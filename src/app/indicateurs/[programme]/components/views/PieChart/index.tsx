import React from "react";

import {
  Chart as ChartJS,
  Title,
  ArcElement,
  Tooltip,
  ChartOptions,
  Legend,
  ChartData,
} from "chart.js";
import { Pie } from "react-chartjs-2";
import { Navigation, A11y } from "swiper/modules";
import { Swiper, SwiperSlide } from "swiper/react";

import "swiper/css";
import "swiper/css/navigation";
import { CHARTJS_OPTIONS } from "@/constants/chart";
import { extractChartData } from "@/lib/chart/extractChartData";
import { extractPieChartsData } from "@/lib/chart/extractPieChartsData";
import { formatNumericValue } from "@/lib/formatNumericValue";
import { IndicateurViewData } from "@/lib/indicateurs/extractIndicateurViewData";
import { hasDimensions } from "@/lib/indicateurs/hasDimensions";
import { Metadata } from "@/types/indicateur";

import styles from "./styles.module.scss";

ChartJS.register(Title, Tooltip, ArcElement, Legend);

ChartJS.defaults.font.family = '"Marianne", arial, sans-serif';

const generateOptions = (unit?: string | null) => {
  const options: ChartOptions<"pie"> = {
    ...CHARTJS_OPTIONS,
    indexAxis: "y" as const,
    plugins: {
      tooltip: {
        xAlign: "left" as const,
        backgroundColor: "#e3e3fd",
        padding: 20,
        bodyColor: "#3a3a3a",
        titleColor: "#3a3a3a",
        callbacks: {
          label: (context) =>
            formatNumericValue({
              value: context.parsed,
              unit,
            }),
        },
      },
    },
  };

  return options;
};

type PieChartProps = {
  data: IndicateurViewData[];
  definition: Metadata;
  chartRef?: React.RefObject<ChartJS<"pie", (string | number)[]>>;
};

type PieSlideProps = {
  label: string;
  chartData: ChartData<"pie">;
  options: Record<string, unknown>;
  index: number;
};

const PieSlide = ({ label, chartData, options }: PieSlideProps) => {
  return (
    <div className="fr-p-2w">
      <h6 className={styles.centered}>{label}</h6>
      <div className={styles.container}>
        <Pie data={chartData} options={options} />
      </div>
    </div>
  );
};

const PieCharts = ({ data, definition }: PieChartProps) => {
  const chartsData = extractPieChartsData(data, definition);

  const options = generateOptions(definition.unite);

  return (
    <Swiper
      modules={[Navigation, A11y]}
      slidesPerView={1}
      navigation
      className={styles.swiper}
    >
      {chartsData.map((chartData, index) => {
        return (
          <SwiperSlide key={index}>
            <PieSlide
              label={data[index].label}
              chartData={chartData}
              options={options}
              index={index}
            />
          </SwiperSlide>
        );
      })}
    </Swiper>
  );
};

const PieChart = ({ data, definition, chartRef }: PieChartProps) => {
  const dimensions = hasDimensions(definition);

  if (dimensions) {
    return <PieCharts data={data} definition={definition} />;
  }

  const chartData = extractChartData(data);

  const options = generateOptions(definition.unite);

  return (
    <div className="fr-p-2w">
      <div className={styles.container}>
        <Pie data={chartData} options={options} ref={chartRef} />
      </div>
    </div>
  );
};

export default PieChart;
