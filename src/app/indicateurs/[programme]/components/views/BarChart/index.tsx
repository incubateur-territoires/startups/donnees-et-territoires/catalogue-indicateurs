/* eslint-disable @typescript-eslint/no-explicit-any */
import React from "react";

import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  ChartOptions,
  ActiveElement,
} from "chart.js";
import { Bar } from "react-chartjs-2";

import { CHARTJS_OPTIONS } from "@/constants/chart";
import { extractBarChartsData } from "@/lib/chart/extractBarChartsData";
import { extractChartData } from "@/lib/chart/extractChartData";
import { formatNumericValue } from "@/lib/formatNumericValue";
import { getDSFRHexaFromName } from "@/lib/getDSFRHexaFromName";
import { IndicateurViewData } from "@/lib/indicateurs/extractIndicateurViewData";
import { hasDimensions } from "@/lib/indicateurs/hasDimensions";
import { Metadata } from "@/types/indicateur";

ChartJS.register(CategoryScale, LinearScale, BarElement, Title, Tooltip);

ChartJS.defaults.font.family = '"Marianne", arial, sans-serif';

(Tooltip.positioners as any).center = function (items: ActiveElement[]) {
  const item = items[0];
  if (!item) {
    return false;
  }

  const x = item.element.x - (item.element as any).width / 2;

  return {
    x,
    y: item.element.y,
    xAlign: "center",
    yAlign: "bottom",
  };
};

const generateOptions = (stacked: boolean, unit?: string | null) => {
  const options: ChartOptions<"bar"> = {
    ...CHARTJS_OPTIONS,
    indexAxis: "y" as const,
    plugins: {
      legend: {
        display: false,
      },
      tooltip: {
        position: "center" as any,
        padding: 20,
        backgroundColor: "#e3e3fd",
        bodyColor: "#3a3a3a",
        titleColor: "#3a3a3a",
        callbacks: {
          ...(stacked
            ? {
                title: (context) => {
                  return context[0].dataset.label;
                },
              }
            : {}),

          label: (context) => {
            return formatNumericValue({
              value: context.parsed.x,
              unit,
            });
          },
        },
      },
    },
    scales: {
      x: {
        stacked,
        ticks: {
          callback: (value: number | string) =>
            formatNumericValue({ value, unit }),
          color: getDSFRHexaFromName("1000", "grey", "50"),
        },
      },
      y: {
        stacked,
        ticks: {
          z: 1,
          color: getDSFRHexaFromName("1000", "grey", "50"),
        },
      },
    },
  };

  return options;
};

type BarChartProps = {
  data: IndicateurViewData[];
  definition: Metadata;
  chartRef?: React.RefObject<ChartJS<"bar", (string | number)[]>>;
};

const StackedBarChart = ({ data, definition }: BarChartProps) => {
  const chartsData = extractBarChartsData(data, definition);

  const options = generateOptions(true, definition.unite);

  return (
    <div className="fr-p-2w">
      <Bar data={chartsData} options={options} />
    </div>
  );
};

const BarChart = ({ data, definition, chartRef }: BarChartProps) => {
  const dimensions = hasDimensions(definition);

  if (dimensions) {
    return <StackedBarChart data={data} definition={definition} />;
  }

  const chartData = extractChartData(data);

  const options = generateOptions(false, definition.unite);

  return (
    <div className="fr-p-2w">
      <Bar data={chartData} options={options} ref={chartRef} />
    </div>
  );
};

export default BarChart;
