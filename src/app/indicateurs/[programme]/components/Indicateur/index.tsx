"use client";

import React, { useMemo, useRef, useState } from "react";

import Button from "@codegouvfr/react-dsfr/Button";
import { ButtonsGroup } from "@codegouvfr/react-dsfr/ButtonsGroup";
import { createModal } from "@codegouvfr/react-dsfr/Modal";
import Notice from "@codegouvfr/react-dsfr/Notice";
import { Tabs } from "@codegouvfr/react-dsfr/Tabs";
import { Chart } from "chart.js";
import clsx from "clsx";
import Image from "next/image";

import { navigate } from "@/app/actions";
import ComputationModal from "@/components/ComputationModal";
import CopyToClipboardButton from "@/components/CopyToClipboardButton";
import MdxAssistantModal from "@/components/MdxAssistantModal";
import Tooltip from "@/components/Tooltip";
import { useTerritoiresOptionsContext } from "@/context/TerritoiresOptionsContext";
import { fetchDatasets } from "@/lib/api/datasets";
import { writeToClipboard } from "@/lib/copy/writeToClipboard";
import { extractQueryTerritoires } from "@/lib/extractQueryTerritoires";
import { getAvailableViews } from "@/lib/getAvailableViews";
import { getMailleLabelFromQuery } from "@/lib/getMailleLabelFromQuery";
import { getUpdatedAt } from "@/lib/getUpdatedAt";
import {
  IndicateurViewData,
  extractIndicateurViewData,
} from "@/lib/indicateurs/extractIndicateurViewData";
import { getIndicateurType } from "@/lib/indicateurs/getIndicateurType";
import { DefinitionIndicateur, Metadata } from "@/types/indicateur";

import styles from "./styles.module.scss";
import BarChart from "../views/BarChart";
import Map from "../views/Map";
import PieChart from "../views/PieChart";
import Table from "../views/Table";

type IndicateurData = { data?: DefinitionIndicateur; isLoading: boolean }[];

type IndicateurProps = {
  definition: Metadata;
  data: IndicateurData;
  territoires: string;
};

const Indicateur = ({ data, definition, territoires }: IndicateurProps) => {
  const computationModal = useMemo(
    () =>
      createModal({
        id: `source-modal-${definition.identifiant}`,
        isOpenedByDefault: false,
      }),
    [definition.identifiant],
  );

  const mdxAssistantModal = useMemo(
    () =>
      createModal({
        id: `mdx-assistant-modal-${definition.identifiant}`,
        isOpenedByDefault: false,
      }),
    [definition.identifiant],
  );

  const updatedAt =
    getUpdatedAt(definition.datasets)?.toLocaleDateString("fr-FR", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
    }) ?? "-";

  const parsedTerritories = extractQueryTerritoires(territoires);

  const unavailableTerritories = parsedTerritories
    .filter(
      (territory) =>
        !definition.mailles.some((maille) => maille.startsWith(territory[0])),
    )
    .map((territory) => getMailleLabelFromQuery(territory[0]));

  const territoiresOptions = useTerritoiresOptionsContext();

  const [view, setView] = useState("table");

  const pieChartRef = useRef<Chart<"pie", (string | number)[]>>(null);
  const barChartRef = useRef<Chart<"bar", (string | number)[]>>(null);
  const mapRef = useRef<HTMLDivElement>(null);

  const type = getIndicateurType(data?.[0]?.data);

  const handleSourceClick = (event: React.MouseEvent) => {
    event.preventDefault();

    computationModal.open();
  };

  const handleMdxClick = (event: React.MouseEvent) => {
    event.preventDefault();
    mdxAssistantModal.open();
  };

  const [copierText, setCopierText] = useState("Copier l’identifiant");

  const viewData: IndicateurViewData[] = useMemo(
    () =>
      data
        .filter((entry) => entry.isLoading || entry.data)
        .map((entry) =>
          extractIndicateurViewData(
            entry.data,
            entry.isLoading,
            territoiresOptions,
          ),
        ),
    [data, territoiresOptions],
  );

  const availableViews = getAvailableViews(type);

  const navigateToDatasetsPage = async () => {
    const datasets = await fetchDatasets();
    const datasetList = datasets.filter((dataset) => {
      return dataset.indicateurs.find(
        (indicateur) =>
          definition.identifiant === indicateur.metadata.identifiant,
      );
    });
    if (datasetList.length === 1) {
      navigate(`/donnees/${datasetList[0].table}`);
    } else {
      navigate(`/donnees?indicateurs=${definition.identifiant}`);
    }
  };

  return (
    <>
      <ComputationModal modal={computationModal} definition={definition} />
      <MdxAssistantModal
        modal={mdxAssistantModal}
        definition={definition}
        data={data.find((entry) => entry.data)?.data}
      />

      <div key={definition.identifiant} className="fr-mb-6w">
        <div className={styles["block-title-container"]}>
          <div>
            <h2 className={clsx("fr-h6 fr-mb-1v", styles.title)}>
              <span
                aria-hidden="true"
                className={clsx(
                  "ri-bar-chart-fill fr-mr-1w",
                  styles["bar-chart-icon"],
                )}
              />
              {definition.nom}
            </h2>
            <div className={styles["metadata"]}>
              <span className="fr-text--sm fr-mb-0">
                Mise à jour le {updatedAt}
              </span>
              <button
                className={clsx(
                  "fr-link fr-text--sm fr-icon-arrow-right-line fr-link--icon-right fr-ml-2w",
                  styles["bottom-link-source"],
                )}
                onClick={navigateToDatasetsPage}
              >
                Source
              </button>
            </div>
          </div>
          {viewData.length > 0 && (
            <CopyToClipboardButton
              className={styles.copy}
              data={viewData}
              definition={definition}
              view={view}
              chartRef={view === "pie" ? pieChartRef : barChartRef}
              mapRef={mapRef}
            />
          )}
        </div>
        <p className="fr-text--md">{definition.description}</p>
        {unavailableTerritories.length > 0 && (
          <Notice
            className="fr-mb-2w"
            title={
              <>
                Cet indicateur n’est pas disponible pour les territoires
                suivants : {unavailableTerritories.join(", ")}
              </>
            }
          />
        )}
        {viewData.length === 0 && (
          <div
            className={clsx("fr-p-3w", styles["no-territories-selected-box"])}
          >
            <p className="fr-text--md fr-mb-0 ">
              Vous n’avez sélectionné aucun territoire
            </p>
            <p className="fr-text--xs fr-mb-0">
              Les valeurs de l’indicateur s’afficheront ici
            </p>
            <Image
              src="/icons/no-result.svg"
              alt=""
              width={30}
              height={43}
              className={styles["absolute-centered"]}
            />
          </div>
        )}
        {viewData.length > 0 && (
          <Tabs
            tabs={availableViews.map((view, _index) => ({
              tabId: view.id,
              label: view.label,
            }))}
            selectedTabId={view}
            onTabChange={(tabId) => setView(tabId)}
          >
            <div>
              {view == "table" && (
                <Table data={viewData} definition={definition} />
              )}
              {view == "pie" && (
                <PieChart
                  data={viewData}
                  definition={definition}
                  chartRef={pieChartRef}
                />
              )}
              {view == "bar" && (
                <BarChart
                  data={viewData}
                  definition={definition}
                  chartRef={barChartRef}
                />
              )}
              {view == "map" && <Map data={viewData} mapRef={mapRef} />}
            </div>
          </Tabs>
        )}
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "flex-start",
          }}
          className="fr-ml-1v"
        >
          <ButtonsGroup
            buttons={[
              {
                children: "Calcul",
                iconId: "ri-calculator-line",
                onClick: handleSourceClick,
                priority: "tertiary no outline",
                className: styles["bottom-link"],
              },
              {
                children: "Fiche",
                iconId: "ri-magic-line",
                onClick: handleMdxClick,
                priority: "tertiary no outline",
                className: styles["bottom-link"],
              },
            ]}
            inlineLayoutWhen="always"
          />
          <Tooltip
            title={copierText}
            arrow
            // Disable regular hover / focus opening
            onOpen={() => {}}
            onClose={() => {
              setTimeout(() => setCopierText("Copier l’identifiant"), 500);
            }}
            placement="right"
          >
            <Button
              className={styles["bottom-link"]}
              onClick={(event) => {
                event.preventDefault();
                writeToClipboard(
                  definition.identifiant,
                  definition.identifiant,
                );
                setCopierText("Identifiant copié !");
              }}
              priority="tertiary no outline"
            >
              id = {definition.identifiant}
            </Button>
          </Tooltip>
        </div>
      </div>
    </>
  );
};

export default Indicateur;
