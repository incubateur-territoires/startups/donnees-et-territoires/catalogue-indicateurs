import React from "react";

import { UseQueryResult } from "@tanstack/react-query";
import clsx from "clsx";
import Link from "next/link";

import CopyAllToClipboardButton from "@/components/CopyAllToClipboardButton";
import CopyLinkToClipboardButton from "@/components/CopyLinkToClipboardButton";
import { NetworkError } from "@/lib/api/NetworkError";
import { DefinitionIndicateur, Metadata } from "@/types/indicateur";
import { Programme } from "@/types/programme";

import styles from "./styles.module.scss";

type PageTitleProps = {
  className?: string;
  title: string;
  searchTerm?: string;
  definitions: Metadata[];
  data: UseQueryResult<DefinitionIndicateur[], NetworkError>[];
  programme?: Programme;
};

const PageTitle = ({
  className,
  title,
  searchTerm,
  definitions,
  data,
  programme,
}: PageTitleProps) => {
  return (
    <section className={clsx("fr-pb-4w", styles["title-container"], className)}>
      <div>
        <h1 className={clsx("fr-h3 fr-mb-0", styles["title"])}>
          <span
            aria-hidden="true"
            className={clsx(
              "ri-bar-chart-fill fr-mr-1w",
              styles["bar-chart-icon"],
            )}
          />
          {title}
        </h1>
        <Link
          className="fr-link fr-text--sm"
          href={`/donnees?programmes=${programme?.identifiant}`}
        >
          Voir toutes les sources de données
        </Link>
      </div>
      <div className={clsx(styles.actions)}>
        <CopyAllToClipboardButton definitions={definitions} data={data} />
        <CopyLinkToClipboardButton searchTerm={searchTerm || ""} />
      </div>
    </section>
  );
};

export default PageTitle;
