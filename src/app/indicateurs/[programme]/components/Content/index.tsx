"use client";

import React, { useEffect, useMemo, useState } from "react";

import clsx from "clsx";
import debounce from "lodash/debounce";
import Image from "next/image";

import { HIDDEN_METRICS } from "@/constants/indicateurs";
import { TerritoiresOptionsProvider } from "@/context/TerritoiresOptionsContext";
import { useFetchIndicateurs } from "@/hooks/useFetchIndicateurs";
import { useFuzzySearch } from "@/hooks/useFuzzySearch";
import { Metadata } from "@/types/indicateur";
import { Programme } from "@/types/programme";
import { TerritoiresOptions } from "@/types/select";

import styles from "./styles.module.scss";
import FiltersCaracteristics from "../Filters/Caracteristics";
import FiltersTerritoires from "../Filters/Territoires";
import List from "../List";
import PageTitle from "../PageTitle";

export const SEARCH_DELAY = 400;

export const FUSE_OPTIONS = {
  keys: [{ name: "nom", weight: 2 }, "identifiant"],
  threshold: 0.35,
};

type ContentProps = {
  indicateurs: Metadata[];
  programme?: Programme;
  territoiresOptions: TerritoiresOptions;
  territoires: string;
  search: string;
};

const Content = ({
  programme,
  indicateurs,
  territoiresOptions,
  territoires,
  search,
}: ContentProps) => {
  const [searchState, setSearchState] = useState({
    term: search,
    filter: search,
  });
  const [isCommuneLevel, setIsCommuneLevel] = useState(false);

  const debouncedSetSearchFilter = useMemo(
    () =>
      debounce((term) => {
        setSearchState((prevState) => ({ ...prevState, filter: term }));
      }, SEARCH_DELAY),
    [],
  );

  const clearSearch = () => {
    setSearchState({
      term: "",
      filter: "",
    });
  };

  const setSearchTerm = (term: string) => {
    setSearchState((prevState) => ({ ...prevState, term }));
  };

  useEffect(() => {
    debouncedSetSearchFilter(searchState.term);
  }, [debouncedSetSearchFilter, searchState.term]);

  const fuzzySearchResults = useFuzzySearch(
    searchState.filter,
    indicateurs,
    FUSE_OPTIONS,
  );
  const getVisibleIndicateurs = () => {
    let results = [...indicateurs];
    if (searchState.filter.length > 0) {
      results = fuzzySearchResults;
    }
    if (isCommuneLevel) {
      results = results.filter((metric) => metric.mailles.includes("commune"));
    }
    return results;
  };

  const visibleIndicateurs = getVisibleIndicateurs();

  const { data, error } = useFetchIndicateurs({
    territoires,
    indicateurs,
  });

  const filteredIndicateursCount = visibleIndicateurs.filter(
    (metric) => !HIDDEN_METRICS.includes(metric.identifiant),
  ).length;

  const totalIndicateursCount = indicateurs.filter(
    (metric) => !HIDDEN_METRICS.includes(metric.identifiant),
  ).length;

  return (
    <TerritoiresOptionsProvider options={territoiresOptions}>
      <PageTitle
        className={clsx("fr-col-12", styles.title)}
        title={programme?.nom || ""}
        searchTerm={searchState.term}
        definitions={visibleIndicateurs}
        data={data}
        programme={programme}
      />
      <section
        className={clsx(
          "fr-col-12 fr-py-4w fr-pb-6w fr-px-1w fr-px-md-0",
          styles["selection-territoires"],
        )}
      >
        <FiltersTerritoires
          programme={programme}
          territoiresOptions={territoiresOptions}
          territoires={territoires}
          searchTerm={searchState.term}
        />
        <div className={styles["france-image-container"]}>
          <Image
            src="/icons/france-points.svg"
            alt=""
            width={471}
            height={311}
            className={styles["france-image"]}
          />
        </div>
      </section>
      <div className={clsx("fr-col-12 fr-col-lg-8", styles.main)}>
        <div
          className={clsx(
            styles["nbre-indicateur"],
            "fr-pb-2w fr-pt-3w fr-pr-4w",
          )}
        >
          {filteredIndicateursCount !== totalIndicateursCount
            ? `${filteredIndicateursCount} indicateurs affichés sur `
            : ""}
          {totalIndicateursCount} indicateurs disponibles
        </div>
        <List
          error={error}
          definitions={visibleIndicateurs}
          territoires={territoires}
          data={data}
        />
      </div>
      <FiltersCaracteristics
        searchTerm={searchState.term}
        setSearchTerm={setSearchTerm}
        clearSearch={clearSearch}
        isCommuneLevel={isCommuneLevel}
        setIsCommuneLevel={setIsCommuneLevel}
      />
    </TerritoiresOptionsProvider>
  );
};

export default Content;
