import React from "react";

import { Breadcrumb } from "@codegouvfr/react-dsfr/Breadcrumb";

import { fetchDefinitions } from "@/lib/api/indicateurs";
import { fetchProgrammes } from "@/lib/api/programmes";
import { fetchTerritoires } from "@/lib/api/territoires";

import Content from "./components/Content";

type IndicateursProps = {
  params: Promise<{
    programme: string;
  }>;
  searchParams: Promise<{
    territoires?: string;
    search?: string;
  }>;
};

const Indicateurs = async (props: IndicateursProps) => {
  const searchParams = await props.searchParams;
  const { programme: param_programme } = await props.params;
  const { territoires, search } = searchParams;

  const programmes = fetchProgrammes();

  const territoiresOptions = await fetchTerritoires();

  const programme = programmes.find(
    (programme) => programme.identifiant === param_programme,
  );

  const indicateurs = await fetchDefinitions();

  const matchingIndicateurs = indicateurs.filter((indicateur) =>
    indicateur.tags.some((tag) => tag.value === programme?.identifiant),
  );

  return (
    <main className="fr-grid-row fr-container">
      <Breadcrumb
        className="fr-col-12"
        currentPageLabel="Indicateurs"
        homeLinkProps={{
          href: "/",
        }}
        segments={[]}
      />
      <Content
        search={search || ""}
        indicateurs={matchingIndicateurs}
        programme={programme}
        territoiresOptions={territoiresOptions}
        territoires={territoires || ""}
      />
    </main>
  );
};

export default Indicateurs;
