import { Suspense } from "react";

import Header from "@/components/Header";
import { ListModalProvider } from "@/context/ListModalContext";

export default function IndicateursLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <ListModalProvider>
      <Suspense fallback={<div>Chargement...</div>}>
        <Header />
      </Suspense>
      {children}
    </ListModalProvider>
  );
}
