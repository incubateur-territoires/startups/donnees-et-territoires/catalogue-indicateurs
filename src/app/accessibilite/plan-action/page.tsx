export default function PlanAction() {
  return (
    <main className="fr-container fr-py-4w">
      <h1 className="fr-h1">Plan d’action d’accessibilité</h1>
      <p>
        Le plan d’action détaille les actions menées par Données et Territoires
        pour améliorer continuellement l’accessibilité du catalogue
        d’indicateurs. Dernière mise à jour le 20 novembre 2024.
      </p>

      <h2 className="fr-h2">Actions en cours et à venir en 2025</h2>
      <ul>
        <li>
          Utiliser les résultats de l’Audit interne d’accessibilité des Fiches
          Territoriales pour améliorer l’accessibilité du catalogue.
        </li>
        <li>Amélioration continue</li>
      </ul>
    </main>
  );
}
