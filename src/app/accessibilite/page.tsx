import Link from "next/link";

export default async function Accessibilite() {
  return (
    <main className="fr-container fr-py-4w">
      <h1>Déclaration d’accessibilité</h1>
      <p>
        Établie le <span>9 novembre 2023</span>.
      </p>
      <p>
        <span>l’Agence Nationale de la Cohésion des Territoires</span> s’engage
        à rendre son service accessible, conformément à l’article 47 de la loi
        n° 2005-102 du 11 février 2005.
      </p>
      <div>
        À cette fin, la stratégie et les actions suivantes sont mis en œuvre :
        <ul>
          <li>
            <a
              href="https://grist.numerique.gouv.fr/o/docs/wjdVsJW7ArgH/Schema-pluriannuels"
              target="_blank"
              rel="noreferrer"
              title="Schéma pluriannuel de mise en accessibilité de Beta Gouv - Ouvre une nouvelle fenêtre"
            >
              Schéma pluriannuel de mise en accessibilité de Beta Gouv
            </a>
          </li>
          <li>
            <Link href="/accessibilite/plan-action" key="a11y-plan-action">
              Plan d’action année 2025
            </Link>
          </li>
        </ul>
      </div>
      <p>
        Cette déclaration d’accessibilité s’applique au{" "}
        <a href="https://catalogue-indicateurs.donnees.incubateur.anct.gouv.fr/">
          Catalogue d’indicateurs ANCT
        </a>
        .
      </p>
      <h2>État de conformité</h2>
      <p>
        Le <strong>Catalogue d’indicateurs ANCT</strong> est{" "}
        <strong>
          <span>non conforme</span>
        </strong>{" "}
        avec le RGAA. <span>Le site n’a encore pas été audité.</span>
      </p>
      <h2>Établissement de cette déclaration d’accessibilité</h2>
      <p>
        Cette déclaration a été établie le <span>9 novembre 2023</span>.
      </p>
      <h3>Technologies utilisées</h3>
      <p>
        L’accessibilité du <span>Catalogue d’indicateurs ANCT</span> s’appuie
        sur les technologies suivantes&nbsp;:
      </p>
      <ul>
        <li>HTML</li>
        <li>WAI-ARIA</li>
        <li>CSS</li>
        <li>JavaScript</li>
      </ul>
      <h2 className="fr-mt-2w">Amélioration et contact</h2>
      <p>
        Si vous n’arrivez pas à accéder à un contenu ou à un service, vous
        pouvez contacter le responsable du{" "}
        <span>Catalogue d’indicateurs ANCT</span> pour être orienté vers une
        alternative accessible ou obtenir le contenu sous une autre forme.
      </p>
      <ul>
        <li>
          E-mail&nbsp;:{" "}
          <a href="mailto:donnees@anct.gouv.fr">donnees@anct.gouv.fr</a>
        </li>
      </ul>
      <p>
        Nous essayons de répondre dans les <span>2 jours ouvrés</span>.
      </p>
      <h2>Voie de recours</h2>
      <p>
        Cette procédure est à utiliser dans le cas suivant&nbsp;: vous avez
        signalé au responsable du site internet un défaut d’accessibilité qui
        vous empêche d’accéder à un contenu ou à un des services du portail et
        vous n’avez pas obtenu de réponse satisfaisante.
      </p>
      <p>Vous pouvez&nbsp;:</p>
      <ul>
        <li>
          Écrire un message au{" "}
          <a href="https://formulaire.defenseurdesdroits.fr/">
            Défenseur des droits
          </a>
        </li>
        <li>
          Contacter{" "}
          <a href="https://www.defenseurdesdroits.fr/saisir/delegues">
            le délégué du Défenseur des droits dans votre région
          </a>
        </li>
        <li>
          Envoyer un courrier par la poste (gratuit, ne pas mettre de
          timbre)&nbsp;:
          <br />
          Défenseur des droits
          <br />
          Libre réponse 71120 75342 Paris CEDEX 07
        </li>
      </ul>
    </main>
  );
}
