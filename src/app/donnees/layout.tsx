import { Suspense } from "react";

import Header from "@/components/Header";

export default function IndicateursLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <>
      <Suspense fallback={<div>Chargement...</div>}>
        <Header />
      </Suspense>
      {children}
    </>
  );
}
