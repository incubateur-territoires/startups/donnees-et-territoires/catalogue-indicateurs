import React from "react";

import clsx from "clsx";
import Markdown from "react-markdown";

import Producer from "@/components/Producer";
import SourceInfo from "@/components/SourceInfo";
import { remarkDsfrLinks } from "@/lib/remark/remarkDsfrLinks";
import { DatasetWithIndicateurs } from "@/types/schema";

import styles from "./styles.module.scss";

type ContentProps = {
  dataset: DatasetWithIndicateurs;
};

const Content = ({ dataset }: ContentProps) => {
  return (
    <section className={clsx("fr-col-12 fr-col-lg-8 fr-mb-6w fr-pr-10w")}>
      <h3 className={clsx("fr-mb-2w", styles.title)}>{dataset.name}</h3>
      {dataset.producer?.name && <Producer producer={dataset.producer} />}

      {dataset.description && (
        <div>
          <Markdown remarkPlugins={[remarkDsfrLinks]}>
            {dataset.description}
          </Markdown>
        </div>
      )}

      <div className="fr-mt-2w fr-mb-1w fr-text--lg fr-text--bold">
        Ces données sont produites à partir de :
      </div>
      <SourceInfo dataset={dataset} />
    </section>
  );
};

export default Content;
