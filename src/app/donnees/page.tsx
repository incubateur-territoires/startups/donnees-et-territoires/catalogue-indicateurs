import React from "react";

import { Breadcrumb } from "@codegouvfr/react-dsfr/Breadcrumb";

import { fetchDatasets } from "@/lib/api/datasets";
import { fetchDefinitions } from "@/lib/api/indicateurs";
import { fetchProgrammes } from "@/lib/api/programmes";

import Content from "./components/Content";

type DatasetsProps = {
  searchParams: Promise<{
    search?: string;
    indicateurs?: string;
    programmes?: string;
  }>;
};

const Datasets = async (props: DatasetsProps) => {
  const searchParams = await props.searchParams;
  const { search, indicateurs, programmes } = searchParams;

  const datasets = await fetchDatasets();
  const definitions = await fetchDefinitions();
  const programmesOptions = fetchProgrammes();

  return (
    <main className="fr-grid-row fr-container">
      <Breadcrumb
        className="fr-col-12"
        currentPageLabel="Données"
        homeLinkProps={{
          href: "/",
        }}
        segments={[]}
      />
      <Content
        search={search || ""}
        datasets={datasets}
        definitions={definitions}
        indicateurs={indicateurs || ""}
        programmes={programmes || ""}
        programmesOptions={programmesOptions}
      />
    </main>
  );
};

export default Datasets;
