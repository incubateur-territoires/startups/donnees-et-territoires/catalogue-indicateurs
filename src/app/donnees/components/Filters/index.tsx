import React, { ChangeEvent, useCallback, useMemo } from "react";

import Tag from "@codegouvfr/react-dsfr/Tag";
import clsx from "clsx";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { MultiValue } from "react-select";

import Select from "@/components/Select";
import { PROGRAMME_TAG_NAME } from "@/constants/indicateurs";
import { COMMA_SEPARATOR } from "@/constants/separators";
import { useSearchManager } from "@/hooks/useSearchManager";
import { generateDatasetsListPath } from "@/lib/path/generateDatasetsListPath";
import { extractSelectValues } from "@/lib/select/extractSelectValue";
import { Metadata } from "@/types/indicateur";
import { Programme } from "@/types/programme";
import { Option } from "@/types/select";

import styles from "./styles.module.scss";

type FiltersProps = {
  searchTerm: string;
  setSearchTerm: (term: string) => void;
  clearSearch: () => void;
  definitions: Metadata[];
  indicateurs: string;
  programmes: string;
  programmesOptions: Programme[];
};

const filterOption = () => true;

export default function Filters({
  searchTerm,
  setSearchTerm,
  clearSearch,
  definitions,
  indicateurs,
  programmes,
  programmesOptions,
}: FiltersProps) {
  const router = useRouter();

  const extendedDefinitions = useMemo(() => {
    return definitions.map((definition) => {
      const programme = definition.tags
        .filter((tag) => tag.name === PROGRAMME_TAG_NAME && Boolean(tag.nom))
        .map((tag) => tag.nom)
        .join(", ");

      return {
        ...definition,
        nom: `${definition.nom}${programme ? ` (${programme})` : ""}`,
      };
    });
  }, [definitions]);

  const {
    onInputChange: onIndicateursInputChange,
    options: filteredIndicateursOptions,
    searchTerm: metricsSearchTerm,
  } = useSearchManager(extendedDefinitions);

  const {
    onInputChange: onProgrammesInputChange,
    options: filteredProgrammesOptions,
    searchTerm: programmesSearchTerm,
  } = useSearchManager(programmesOptions);

  const handleSearchTermChange = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      setSearchTerm(event.target.value);
    },
    [setSearchTerm],
  );

  const indicateursValues = useMemo(() => {
    const selected = indicateurs.split(COMMA_SEPARATOR);

    return extractSelectValues(filteredIndicateursOptions, (option) =>
      selected.some((identifiant) => option.value === identifiant),
    );
  }, [filteredIndicateursOptions, indicateurs]);

  const programmesValues = useMemo(() => {
    const selected = programmes.split(COMMA_SEPARATOR);

    return extractSelectValues(filteredProgrammesOptions, (option) =>
      selected.some((identifiant) => option.value === identifiant),
    );
  }, [filteredProgrammesOptions, programmes]);

  const onIndicateurTagClose = (option: Option) => {
    const updatedValues = indicateursValues.filter(
      (value) => value.value !== option.value,
    );

    const path = generateDatasetsListPath({
      indicateurs: updatedValues,
      programmes: programmesValues,
    });

    router.push(path, {
      scroll: false,
    });
  };

  const onProgrammeTagClose = (option: Option) => {
    const updatedValues = programmesValues.filter(
      (value) => value.value !== option.value,
    );

    const path = generateDatasetsListPath({
      indicateurs: indicateursValues,
      programmes: updatedValues,
    });

    router.push(path, {
      scroll: false,
    });
  };

  const onIndicateursChange = (values: MultiValue<Option>) => {
    const path = generateDatasetsListPath({
      indicateurs: values,
      programmes: programmesValues,
    });

    router.push(path, {
      scroll: false,
    });
  };

  const onProgrammesChange = (values: MultiValue<Option>) => {
    const path = generateDatasetsListPath({
      indicateurs: indicateursValues,
      programmes: values,
    });

    router.push(path, {
      scroll: false,
    });
  };

  return (
    <div className={clsx("fr-col-12 fr-col-lg-4 fr-py-0", styles.container)}>
      <nav
        className="fr-sidemenu fr-sidemenu--right fr-sidemenu--sticky-full-height fr-pl-0"
        role="navigation"
      >
        <div className={clsx("fr-sidemenu__inner", styles.content)}>
          <button
            className="fr-sidemenu__btn"
            hidden
            aria-controls="fr-sidemenu-wrapper"
            aria-expanded="false"
          >
            Filtrer
          </button>
          <div>
            <div className={clsx("fr-text--lg", styles.weighted)}>
              Filtrer par programme
            </div>
            <div className={styles["search-container"]}>
              <Select<
                {
                  value: string;
                  nom?: string | null;
                  label: string;
                },
                true
              >
                onChange={onProgrammesChange}
                value={programmesValues}
                isMulti
                filterOption={filterOption}
                onInputChange={onProgrammesInputChange}
                inputValue={programmesSearchTerm}
                options={filteredProgrammesOptions}
                instanceId="metrics-search"
                name="search"
                placeholder="Rechercher"
                classNames={{
                  control: () => styles.control,
                  valueContainer: () => styles["value-container"],
                }}
                containerClassName={clsx(
                  "fr-mb-1w",
                  styles["select-container"],
                )}
              />
              <div>
                {programmesValues.map((value) => (
                  <Tag
                    className="fr-mr-1w"
                    key={value.value}
                    dismissible
                    nativeButtonProps={{
                      onClick: () => {
                        onProgrammeTagClose(value);
                      },
                    }}
                    small
                  >
                    {value.label.length > 20
                      ? `${value.label.slice(0, 20)}…`
                      : value.label}
                  </Tag>
                ))}
              </div>
            </div>
          </div>
          <div className="fr-mt-3w">
            <div className={clsx("fr-text--lg", styles.weighted)}>
              Filtrer par indicateur
            </div>
            <div className={styles["search-container"]}>
              <Select<
                {
                  value: string;
                  nom?: string | null;
                  tags: { name: string; value: string }[];
                  label: string;
                },
                true
              >
                onChange={onIndicateursChange}
                value={indicateursValues}
                isMulti
                filterOption={filterOption}
                onInputChange={onIndicateursInputChange}
                inputValue={metricsSearchTerm}
                options={filteredIndicateursOptions}
                instanceId="metrics-search"
                name="search"
                placeholder="Rechercher"
                classNames={{
                  control: () => styles.control,
                  valueContainer: () => styles["value-container"],
                }}
                containerClassName={clsx(
                  "fr-mb-1w",
                  styles["select-container"],
                )}
              />

              <div>
                {indicateursValues.map((value) => (
                  <Tag
                    className="fr-mr-1w"
                    key={value.value}
                    dismissible
                    nativeButtonProps={{
                      onClick: () => {
                        onIndicateurTagClose(value);
                      },
                    }}
                    small
                  >
                    {value.label.length > 20
                      ? `${value.label.slice(0, 20)}…`
                      : value.label}
                  </Tag>
                ))}
              </div>
            </div>
          </div>
          <div className="fr-mt-3w">
            <div className={clsx("fr-mb-1w fr-text--lg", styles.weighted)}>
              Filtrer par mot-clé
            </div>
            <div className={styles["search-container"]}>
              <input
                className={clsx("fr-input", styles["search"])}
                placeholder="Rechercher"
                value={searchTerm}
                onChange={handleSearchTermChange}
              />
            </div>
            {searchTerm && (
              <div className={clsx("fr-mt-1w", styles["link-container"])}>
                <Link
                  className="fr-link fr-text--sm"
                  onClick={clearSearch}
                  href="#"
                >
                  Supprimer le filtre
                </Link>
              </div>
            )}
          </div>
        </div>
      </nav>
    </div>
  );
}
