import React from "react";

import clsx from "clsx";
import Link from "next/link";
import Markdown from "react-markdown";

import Producer from "@/components/Producer";
import { formatDate } from "@/lib/formatDate";
import { removeInvalidIndicateurs } from "@/lib/indicateurs/removeInvalidIndicateurs";
import { remarkSkipLinks } from "@/lib/remark/remarkSkipLinks";
import { DatasetWithIndicateurs } from "@/types/schema";

import styles from "./styles.module.scss";

type DatasetViewProps = {
  dataset: DatasetWithIndicateurs;
};

const DatasetView = ({ dataset }: DatasetViewProps) => {
  const indicateurs = removeInvalidIndicateurs(dataset.indicateurs);

  return (
    <section className={clsx("fr-pb-4w", styles.container)}>
      <Link href={`/donnees/${dataset.table}`}>
        <h3 className={clsx("fr-mb-2w", styles.title)}>{dataset.name}</h3>
      </Link>
      {dataset.producer?.name && <Producer producer={dataset.producer} />}

      <Link href={`/donnees/${dataset.table}`}>
        {dataset.description && (
          <div>
            <Markdown remarkPlugins={[remarkSkipLinks]}>
              {dataset.description}
            </Markdown>
          </div>
        )}
        <div className={clsx("fr-mt-2w", styles.actions)}>
          <div>
            <span className="ri-calendar-2-line" /> Mise à jour le{" "}
            <span className="fr-text--bold">
              {dataset.lastModified
                ? formatDate(dataset.lastModified)
                : "––/––/––––"}
            </span>{" "}
            <span className="fr-text--bold">·</span>{" "}
            <span className="ri-line-chart-line" />{" "}
            <span className="fr-text--bold">{indicateurs.length}</span>{" "}
            Indicateur
            {indicateurs.length !== 1 ? "s" : ""}
          </div>
          <span
            className={clsx(
              "fr-icon-arrow-right-line fr-icon--lg",
              styles.arrow,
            )}
          />
        </div>
      </Link>
    </section>
  );
};

export default DatasetView;
