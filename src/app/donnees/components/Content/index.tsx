"use client";

import React, { useEffect, useMemo, useState } from "react";

import clsx from "clsx";
import debounce from "lodash/debounce";

import { COMMA_SEPARATOR } from "@/constants/separators";
import { useFuzzySearch } from "@/hooks/useFuzzySearch";
import { DatasetWithProgrammes } from "@/types/dataset";
import { Metadata } from "@/types/indicateur";
import { Programme } from "@/types/programme";

import styles from "./styles.module.scss";
import Filters from "../Filters";
import List from "../List";

export const SEARCH_DELAY = 400;

export const FUSE_OPTIONS = {
  keys: [{ name: "name", weight: 2 }, "table"],
  threshold: 0.35,
};

type ContentProps = {
  datasets: DatasetWithProgrammes[];
  definitions: Metadata[];
  search: string;
  indicateurs: string;
  programmes: string;
  programmesOptions: Programme[];
};

const Content = ({
  search,
  datasets,
  definitions,
  indicateurs,
  programmes,
  programmesOptions,
}: ContentProps) => {
  const [searchState, setSearchState] = useState({
    term: search,
    filter: search,
  });

  const debouncedSetSearchFilter = useMemo(
    () =>
      debounce((term) => {
        setSearchState((prevState) => ({ ...prevState, filter: term }));
      }, SEARCH_DELAY),
    [],
  );

  const clearSearch = () => {
    setSearchState({
      term: "",
      filter: "",
    });
  };

  const setSearchTerm = (term: string) => {
    setSearchState((prevState) => ({ ...prevState, term }));
  };

  useEffect(() => {
    debouncedSetSearchFilter(searchState.term);
  }, [debouncedSetSearchFilter, searchState.term]);

  const results = useFuzzySearch(searchState.filter, datasets, FUSE_OPTIONS);

  const searchFilteredDatasets =
    searchState.filter.length > 0 ? results : datasets;

  const metricsIds = indicateurs ? indicateurs.split(COMMA_SEPARATOR) : [];
  const programsIds = programmes ? programmes.split(COMMA_SEPARATOR) : [];

  const hasMetricsFilter = metricsIds.length > 0;
  const hasProgramsFilter = programsIds.length > 0;
  const hasFilter = hasMetricsFilter || hasProgramsFilter;

  const visibleDatasets = hasFilter
    ? searchFilteredDatasets.filter((dataset) => {
        const matchesMetricsFilter =
          !hasMetricsFilter ||
          dataset.indicateurs.find((indicateur) =>
            metricsIds.includes(indicateur.metadata.identifiant),
          );

        const matchesProgramsFilter =
          !hasProgramsFilter ||
          dataset.programmes.find((programme) =>
            programsIds.includes(programme),
          );

        return matchesMetricsFilter && matchesProgramsFilter;
      })
    : searchFilteredDatasets;

  return (
    <>
      <div className={clsx("fr-col-12 fr-col-lg-8", styles.main)}>
        <List datasets={visibleDatasets} totalCount={datasets.length} />
      </div>
      <Filters
        searchTerm={searchState.term}
        setSearchTerm={setSearchTerm}
        clearSearch={clearSearch}
        definitions={definitions}
        indicateurs={indicateurs}
        programmesOptions={programmesOptions}
        programmes={programmes}
      />
    </>
  );
};

export default Content;
