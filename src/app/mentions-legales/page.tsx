export default async function Accessibilite() {
  return (
    <main className="fr-container fr-py-4w">
      <h1>Mentions légales</h1>
      <h2>Éditeur de la Plateforme</h2>
      <p>
        Catalogue d’indicateurs est édité au sein de l’Incubateur des
        Territoires de l’Agence nationale de la cohésion des territoires (ANCT)
        située :
        <br />
        20 avenue de Ségur
        <br />
        75 007 Paris
        <br />
        France
        <br />
        <br />
        Téléphone :{" "}
        <a href="tel:01 85 58 60 00" className="fr-link">
          01 85 58 60 00
        </a>
      </p>
      <h2>Directeur de la publication</h2>
      <p>
        Le directeur de publication est Monsieur Stanislas BOURRON, Directeur
        général de l’ANCT
      </p>
      <h2>Hébergement de la Plateforme</h2>
      <p>
        La plateforme est hébergée par :
        <br />
        Scaleway
        <br />
        8 rue de la Ville l’Evêque
        <br />
        75 008 Paris
        <br />
        France
      </p>
      <h2>Accessibilité</h2>
      <p>
        La conformité aux normes d’ccessibilité numérique est un{" "}
        <a href="/accessibilite" className="fr-link">
          objectif ultérieur
        </a>{" "}
        mais nous tâchons de rendre cette plateforme accessible à toutes et à
        tous.
      </p>
      <h2>En savoir plus</h2>
      <p>
        Pour en savoir plus sur la politique d’accessibilité numérique de
        l’État :{" "}
        <a
          className="fr-link"
          href="https://accessibilite.numerique.gouv.fr/"
          target="_blank"
          rel="noreferrer"
        >
          https://accessibilite.numerique.gouv.fr/
        </a>
      </p>
      <h2>Signaler un dysfonctionnement</h2>
      <p>
        Si vous rencontrez un défaut d’accessibilité vous empêchant d’accéder à
        un contenu ou une fonctionnalité de la plateforme, merci de nous en
        faire part :{" "}
        <a className="fr-link" href="mailto: donnees@anct.gouv.fr">
          donnees@anct.gouv.fr
        </a>
        .
      </p>
      <p>
        Si vous n’obtenez pas de réponse rapide de notre part, vous êtes en
        droit de faire parvenir vos doléances ou une demande de saisine au
        Défenseur des Droits.
      </p>
      <h2>Sécurité</h2>
      <p>
        La plateforme est protégée par un certificat électronique, matérialisé
        pour la grande majorité des navigateurs par un cadenas. Cette protection
        participe à la confidentialité des échanges.
      </p>
      <p>
        En aucun cas, les services associés à la plateforme ne seront à
        l’origine d’envoi d’e-mails pour vous demander la saisie d’informations
        personnelles.
      </p>
    </main>
  );
}
