import { MAX_BAR_THICKNESS } from "@/constants/chart";
import { COLORS } from "@/constants/visualization";
import { Dimension, Metadata } from "@/types/schema";

import { getDSFRHexaFromName } from "../getDSFRHexaFromName";
import { IndicateurViewData } from "../indicateurs/extractIndicateurViewData";
import { fillMissingCategories } from "../indicateurs/fillMissingCategories";
import { getCategories } from "../indicateurs/getCategories";

export const extractPieChartsData = (
  data: IndicateurViewData[],
  definition: Metadata,
) => {
  const definitionCategories = getCategories(
    definition.dimensions as Dimension[],
  );
  return data.map((entry) => {
    const categories = fillMissingCategories(
      getCategories(entry.dimensions),
      definitionCategories,
    );

    return categories
      ? {
          labels: categories.map((category) => category.titre),
          datasets: [
            {
              maxBarThickness: MAX_BAR_THICKNESS,
              label: "",
              data: categories.map((category) => category.valeur),
              backgroundColor: categories.map((_category, index) =>
                getDSFRHexaFromName(COLORS[index % 17]),
              ),
            },
          ],
        }
      : {
          labels: [],
          datasets: [
            {
              maxBarThickness: MAX_BAR_THICKNESS,
              label: "",
              data: [],
              backgroundColor: [],
            },
          ],
        };
  });
};
