import { MAX_BAR_THICKNESS } from "@/constants/chart";
import { COLORS } from "@/constants/visualization";
import { Metadata } from "@/types/indicateur";
import { Dimension } from "@/types/schema";

import { getDSFRHexaFromName } from "../getDSFRHexaFromName";
import { IndicateurViewData } from "../indicateurs/extractIndicateurViewData";
import { fillMissingCategories } from "../indicateurs/fillMissingCategories";
import { getCategories } from "../indicateurs/getCategories";

export const extractBarChartsData = (
  data: IndicateurViewData[],
  definition: Metadata,
) => {
  const definitionCategories = getCategories(
    definition.dimensions as Dimension[],
  );
  const baseDatasets = (definitionCategories || []).map((category, index) => ({
    maxBarThickness: MAX_BAR_THICKNESS,
    label: category.titre,
    data: [],
    backgroundColor: getDSFRHexaFromName(COLORS[index % 17]),
  }));

  const { datasets, labels } = data.reduce<{
    labels: string[];
    datasets: {
      maxBarThickness: number;
      label: string;
      data: (string | number)[];
      backgroundColor: string;
    }[];
  }>(
    (chartData, entry) => {
      const { label } = entry;

      const categories = fillMissingCategories(
        getCategories(entry.dimensions),
        definitionCategories,
      );

      (categories || []).forEach((category, index) => {
        chartData.datasets[index].data.push(String(category.valeur));
      });

      chartData.labels.push(String(label));

      return chartData;
    },
    { datasets: baseDatasets, labels: [] },
  );

  return {
    labels,
    datasets,
  };
};
