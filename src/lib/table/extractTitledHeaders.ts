import { AnyOf, IndicateurSchema, Property } from "@/types/indicateur";

export type TitledProperty = {
  title: string;
  type?: "string" | "integer" | "number";
  anyOf?: { type: "null" | "number" | "integer" }[];
  key: string;
};

export const extractTitledHeaders = (schema: IndicateurSchema) => {
  const properties = Object.entries(schema.items?.properties ?? {});

  const keyedProperties = properties.map(([key, property]) => ({
    ...property,
    key,
  }));

  return keyedProperties.filter(
    (property: Property | AnyOf | TitledProperty): property is TitledProperty =>
      Boolean(property.title),
  ) as TitledProperty[];
};
