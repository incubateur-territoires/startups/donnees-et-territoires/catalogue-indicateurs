export function renderUnit(unit?: string | null) {
  if (!unit) {
    return "";
  }

  return unit === "percent" ? "%" : "\u00A0" + unit;
}
