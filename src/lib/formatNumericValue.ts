import { renderUnit } from "./renderUnit";

function isInt(n: number) {
  return n % 1 === 0;
}

/**
 * We consider floats that have no associated unit to be "ratios".
 * This works fine with the current state of our metrics, but this
 * is not a good long term solution.
 *
 * Eventually we will want to "type" our metrics so that we don't
 * have to "guess" how  to display them.
 */
function isPercent(n: number, unit?: string | null) {
  return !isInt(n) && !unit;
}

export const formatNumericValue = ({
  value,
  unit,
  multiplier,
}: {
  value?: string | number | null;
  unit?: string | null;
  multiplier?: number;
}) => {
  if (value === null || value === undefined) {
    return "–";
  }

  if (typeof value === "string") {
    return value;
  }

  if (isPercent(value, unit)) {
    return (
      (value * (multiplier || 100)).toFixed(1).toString().replace(".", ",") +
      "\u00A0%"
    );
  }

  if (unit === "€" && Math.abs(Number(value)) >= 1.0e6) {
    return (
      (Math.abs(Number(value)) / 1.0e6)
        .toFixed(1)
        .toString()
        .replace(".", ",") +
      "\u00A0M" +
      renderUnit(unit)
    );
  }

  return (
    new Intl.NumberFormat("fr-FR").format(Number(value.toFixed(1))) +
    renderUnit(unit)
  );
};
