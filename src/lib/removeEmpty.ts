export const removeEmpty = <T>(list?: T[] | null) =>
  (list || []).filter((item) => {
    const isString = typeof item === "string";

    return (
      (isString && item.length > 0) ||
      (!isString && item !== undefined && item !== null)
    );
  });
