import { COMMA_SEPARATOR } from "@/constants/separators";
import { Option } from "@/types/select";

export function generateDatasetsListPath({
  indicateurs = [],
  programmes = [],
}: {
  indicateurs?: Option[] | readonly Option[];
  programmes?: Option[] | readonly Option[];
}) {
  const searchParams = new URLSearchParams([
    [
      "indicateurs",
      indicateurs.map((indicateur) => indicateur.value).join(COMMA_SEPARATOR),
    ],
    [
      "programmes",
      programmes.map((programme) => programme.value).join(COMMA_SEPARATOR),
    ],
  ]);

  return `/donnees?${searchParams}`;
}
