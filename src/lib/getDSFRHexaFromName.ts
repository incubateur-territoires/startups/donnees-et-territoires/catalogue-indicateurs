export const getDSFRHexaFromName = function (
  colorName: string,
  context?: string,
  use?: string,
) {
  if (typeof window === "undefined") {
    return "";
  }

  return (
    window?.dsfr?.colors?.getColor(
      context || "artwork",
      use || "major",
      colorName,
    ) ?? ""
  );
};
