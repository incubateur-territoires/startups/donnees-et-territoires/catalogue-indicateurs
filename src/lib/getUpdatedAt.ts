import { Dataset } from "@/types/schema";

export const getUpdatedAt = (datasets: Dataset[]) => {
  const dates = datasets
    .map(
      (dataset) =>
        dataset.lastModified && new Date(dataset.lastModified).getTime(),
    )
    .filter((date: number | null): date is number => date !== null);

  if (dates.length === 0) {
    return null;
  }

  return new Date(Math.max.apply(null, dates));
};
