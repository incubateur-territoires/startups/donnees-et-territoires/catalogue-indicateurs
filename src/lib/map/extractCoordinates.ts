import { IndicateurViewData } from "../indicateurs/extractIndicateurViewData";

export const extractCoordinates = (data: IndicateurViewData[]) =>
  data.reduce<{
    bounds: [number, number][];
    departementsCoordinates: [number, number][];
    regionsCoordinates: [number, number][];
  }>(
    (acc, { geoData, maille }) => {
      if (!geoData) {
        return acc;
      }

      const coordinates = geoData.map(({ coordinates }) => coordinates);

      const departementsCoordinates =
        maille === "département"
          ? [...acc.departementsCoordinates, ...coordinates]
          : acc.departementsCoordinates;

      const regionsCoordinates =
        maille === "région"
          ? [...acc.regionsCoordinates, ...coordinates]
          : acc.regionsCoordinates;

      return {
        bounds: [
          ...acc.bounds,
          ...geoData.map(({ coordinates }) => coordinates),
        ],
        departementsCoordinates,
        regionsCoordinates,
      };
    },

    { bounds: [], departementsCoordinates: [], regionsCoordinates: [] },
  );
