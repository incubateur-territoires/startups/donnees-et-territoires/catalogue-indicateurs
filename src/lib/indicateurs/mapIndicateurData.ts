import { UseQueryResult } from "@tanstack/react-query";

import { DefinitionIndicateur } from "@/types/indicateur";
import { IndicateurOneValue } from "@/types/schema";

import { NarrowedTypeIndicateur } from "./typeGuards";
import { NetworkError } from "../api/NetworkError";

type IndicateursData = { data?: DefinitionIndicateur; isLoading: boolean }[];

const OVERRIDES_MAP = {
  logements_vacants: "taux_logements_vacants",
  vacance_commerciale: "taux_vacance_commerciale",
};

/**
 * For ACV vacance metrics, we have two distinct metrics:
 * - One returning the percentage per city for a given territory
 * - One returning the average percentage for the territory
 *
 * However per ACV maintainer's request, we only want to display one metric,
 * showing the average as "value" and having a button to display the detailed list
 *
 * At the moment, insitu cannot merge these two metrics into one as for "rows" type
 * metrics, it can only provide the number of elements in the list as "count". and
 * cannot give us other arbitrary values (like the average we want here).
 *
 * For this reason and until there is an alternative backend side, we are "overriding"
 * the data for the detailed metric with the average instead of the count manually.
 */
const extendTerritoireData = (
  current: DefinitionIndicateur | undefined,
  general: UseQueryResult<DefinitionIndicateur[], NetworkError>,
): DefinitionIndicateur | undefined => {
  if (
    !current ||
    !(current.metadata.identifiant in OVERRIDES_MAP) ||
    !general.data
  ) {
    return current;
  }

  const overrideId =
    OVERRIDES_MAP[current.metadata.identifiant as keyof typeof OVERRIDES_MAP];

  const overrideMetric = general.data.find(
    (metric) => overrideId === metric.metadata.identifiant,
  );

  if (!overrideMetric) {
    return current;
  }

  const mailleKey = Object.keys(current.mailles)[0] as
    | "pays"
    | "departement"
    | "region"
    | "commune";

  const overrideValue: number =
    (overrideMetric.mailles[mailleKey] as IndicateurOneValue)?.valeur / 100;

  const content = (current.mailles?.region ||
    current.mailles?.departement ||
    current.mailles?.pays ||
    {}) as NarrowedTypeIndicateur;

  return {
    ...current,
    metadata: {
      ...current.metadata,
      unite: "%",
    },
    mailles: {
      [mailleKey]: {
        ...content,
        count: overrideValue,
      },
    },
  } as unknown as DefinitionIndicateur;
};

export const mapIndicateurData = (
  id: string,
  data: UseQueryResult<DefinitionIndicateur[], NetworkError>[],
): IndicateursData => {
  return data.reduce<IndicateursData>((accumulator, territoire) => {
    const territoireData = territoire.data?.find(
      (result) => result.metadata.identifiant === id,
    );

    return [
      ...accumulator,
      {
        data: extendTerritoireData(territoireData, territoire),
        isLoading: territoire.isFetching,
      },
    ];
  }, []);
};
