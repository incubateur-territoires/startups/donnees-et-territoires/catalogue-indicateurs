import { Dimension } from "@/types/schema";

import {
  NarrowedTypeIndicateur,
  isList,
  isGeoList,
  isRows,
} from "./typeGuards";

export const getDimensions = (
  indicateur: NarrowedTypeIndicateur,
): null | Dimension[] => {
  if (isList(indicateur) || isRows(indicateur) || isGeoList(indicateur)) {
    return null;
  }

  return indicateur.dimensions;
};
