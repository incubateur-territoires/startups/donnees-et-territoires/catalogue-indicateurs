import { Categorie, Dimension } from "@/types/schema";

export const getCategories = (
  dimensions: Dimension[] | null,
): null | Categorie[] => {
  if (!dimensions || dimensions.length === 0) {
    return null;
  }

  return dimensions[0].categories.sort((a, b) => (a.titre > b.titre ? 1 : -1));
};
