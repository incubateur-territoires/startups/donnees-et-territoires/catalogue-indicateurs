import { DefinitionIndicateur } from "@/types/indicateur";

import { NarrowedTypeIndicateur, isGeoList, isRows } from "./typeGuards";

export const getNarrowedTypeIndicateur = (definition: DefinitionIndicateur) => {
  const { mailles } = definition;
  return (mailles?.region ||
    mailles?.departement ||
    mailles?.pays ||
    {}) as NarrowedTypeIndicateur;
};

export const getIndicateurType = (definition?: DefinitionIndicateur) => {
  if (!definition) {
    return "IndicateurOneValue";
  }

  const content = getNarrowedTypeIndicateur(definition);

  if (isGeoList(content)) {
    return "IndicateurListeGeo";
  }

  if (isRows(content)) {
    return "IndicateurRows";
  }

  return content.__typename || "IndicateurOneValue";
};
