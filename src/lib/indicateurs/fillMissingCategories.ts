import { Categorie } from "@/types/schema";

export const fillMissingCategories = (
  categories: Categorie[] | null,
  definitionCategories: Categorie[] | null,
) => {
  if (!categories || !definitionCategories) {
    return null;
  }

  return definitionCategories.map((definitionCategory) => {
    const match = categories.find(
      (category) => definitionCategory.titre === category.titre,
    );

    return match || { ...definitionCategory, valeur: 0 };
  });
};
