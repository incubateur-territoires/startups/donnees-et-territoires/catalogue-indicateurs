import { Metadata } from "@/types/indicateur";

export const hasDimensions = (definition: Metadata): boolean => {
  return definition.dimensions && definition.dimensions.length > 0;
};
