import { MAILLE_MAP } from "@/constants/mailles";
import { DefinitionIndicateur } from "@/types/indicateur";
import { Dimension } from "@/types/schema";
import { TerritoiresOptions } from "@/types/select";

import { getCount } from "./getCount";
import { getDimensions } from "./getDimensions";
import { GeoData, getGeoData } from "./getGeoData";
import { getIndicateurType } from "./getIndicateurType";
import { getList } from "./getList";
import { NarrowedTypeIndicateur } from "./typeGuards";
import { getMatchingOptions } from "../getMatchingOptions";

export type IndicateurViewData = {
  label: string;
  count: number | string;
  dimensions: Dimension[] | null;
  list: string[] | Record<string, unknown>[] | string | null;
  geoData: GeoData | null;
  isLoading: boolean;
  maille: string;
  hasList: boolean;
};

export const extractIndicateurViewData = (
  data: DefinitionIndicateur | undefined,
  isLoading: boolean,
  territoires: TerritoiresOptions | undefined,
): IndicateurViewData => {
  const type = getIndicateurType(data);

  if (isLoading || !data) {
    return {
      label: "-",
      count: "-",
      dimensions: null,
      list: "-",
      geoData: null,
      isLoading: true,
      maille: "",
      hasList: [
        "IndicateurRows",
        "IndicateurListe",
        "IndicateurListeGeo",
      ].includes(type),
    };
  }

  const { mailles } = data;

  const content = (mailles.region ||
    mailles.departement ||
    mailles.pays) as NarrowedTypeIndicateur;

  const matchingOptions = getMatchingOptions(content, territoires);

  const territoireLabel =
    matchingOptions?.find(
      (option) =>
        option.value === content.code &&
        MAILLE_MAP[option.maille] === content.maille,
    )?.label ?? "-";

  const count = getCount(content);

  const list = getList(content);

  const geoData = getGeoData(content);

  const dimensions = getDimensions(content);

  return {
    label: territoireLabel,
    count,
    dimensions,
    list,
    geoData,
    isLoading,
    maille: content.maille,
    hasList: Boolean(list),
  };
};
