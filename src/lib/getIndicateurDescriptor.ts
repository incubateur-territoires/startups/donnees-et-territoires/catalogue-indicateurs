import { DefinitionIndicateur } from "@/types/indicateur";

import { getNarrowedTypeIndicateur } from "./indicateurs/getIndicateurType";
import {
  isGeoList,
  isList,
  isOneValue,
  isRows,
} from "./indicateurs/typeGuards";

export enum IndicateurDescriptorType {
  INDICATEUR = "Indicateur",
  TAUX = "Taux",
  INDICATEUR_LISTE = "IndicateurListe",
  INDICATEUR_LISTE_LIENS = "IndicateurListeLiens",
  TABLEAU = "Tableau",
}

type IndicateurDescriptorParamType = {
  option: { value: IndicateurDescriptorType; label: string };
  key_id: string;
  key_label: string;
  key_label_liste?: string;
};

export const indicateurDescriptors: Record<
  IndicateurDescriptorType,
  IndicateurDescriptorParamType
> = {
  [IndicateurDescriptorType.INDICATEUR]: {
    option: {
      value: IndicateurDescriptorType.INDICATEUR,
      label: "Valeur simple",
    },
    key_id: "identifiantNumerique",
    key_label: "labelNumerique",
  },
  [IndicateurDescriptorType.TAUX]: {
    option: {
      value: IndicateurDescriptorType.TAUX,
      label: "Taux",
    },
    key_id: "identifiantTaux",
    key_label: "label",
  },
  [IndicateurDescriptorType.INDICATEUR_LISTE]: {
    option: {
      value: IndicateurDescriptorType.INDICATEUR_LISTE,
      label: "Liste de valeurs",
    },
    key_id: "identifiant",
    key_label: "labelNumerique",
    key_label_liste: "labelListe",
  },
  [IndicateurDescriptorType.INDICATEUR_LISTE_LIENS]: {
    option: {
      value: IndicateurDescriptorType.INDICATEUR_LISTE_LIENS,
      label: "Liste de liens",
    },
    key_id: "identifiant",
    key_label: "labelNumerique",
  },
  [IndicateurDescriptorType.TABLEAU]: {
    option: {
      value: IndicateurDescriptorType.TABLEAU,
      label: "Tableau de valeurs",
    },
    key_id: "identifiant",
    key_label: "label",
  },
};

export const getAvailableIndicateurDescriptorOptions = (
  data: DefinitionIndicateur,
) => {
  const mdxIndicateursTypeOptions = [];
  const indicateurType = getNarrowedTypeIndicateur(data);
  if (isOneValue(indicateurType)) {
    mdxIndicateursTypeOptions.push(
      indicateurDescriptors[IndicateurDescriptorType.INDICATEUR].option,
      indicateurDescriptors[IndicateurDescriptorType.TAUX].option,
    );
  }
  if (isList(indicateurType)) {
    mdxIndicateursTypeOptions.push(
      indicateurDescriptors[IndicateurDescriptorType.INDICATEUR].option,
      indicateurDescriptors[IndicateurDescriptorType.INDICATEUR_LISTE].option,
    );
  }
  if (isRows(indicateurType) || isGeoList(indicateurType)) {
    mdxIndicateursTypeOptions.push(
      indicateurDescriptors[IndicateurDescriptorType.INDICATEUR_LISTE].option,
      indicateurDescriptors[IndicateurDescriptorType.INDICATEUR_LISTE_LIENS]
        .option,
      indicateurDescriptors[IndicateurDescriptorType.TABLEAU].option,
    );
  }
  return mdxIndicateursTypeOptions;
};
