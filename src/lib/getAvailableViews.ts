import { FrIconClassName, RiIconClassName } from "@codegouvfr/react-dsfr";

import { Visualization } from "@/types/visualization";

export type IndicateurType =
  | "IndicateurRows"
  | "IndicateurOneValue"
  | "IndicateurListe"
  | "IndicateurListeGeo"
  | "IndicateurRow";

const VIEWS: {
  id: Visualization;
  label: string;
  types: (
    | "IndicateurOneValue"
    | "IndicateurListe"
    | "IndicateurRows"
    | "IndicateurListeGeo"
  )[];
  configuration?: Record<string, unknown>;
  icon: FrIconClassName | RiIconClassName;
}[] = [
  {
    id: "table",
    label: "Tableau",
    types: [
      "IndicateurOneValue",
      "IndicateurListe",
      "IndicateurRows",
      "IndicateurListeGeo",
    ],
    icon: "ri-table-fill",
  },
  {
    id: "bar",
    label: "Histogramme",
    types: [
      "IndicateurOneValue",
      "IndicateurListe",
      "IndicateurRows",
      "IndicateurListeGeo",
    ],
    icon: "ri-bar-chart-box-fill",
  },
  {
    id: "pie",
    label: "Graphique circulaire",
    types: [
      "IndicateurOneValue",
      "IndicateurListe",
      "IndicateurRows",
      "IndicateurListeGeo",
    ],
    icon: "ri-pie-chart-2-fill",
  },
  {
    id: "map",
    label: "Carte",
    types: ["IndicateurListeGeo"],
    icon: "fr-icon-france-fill",
  },
];

export const getAvailableViews = (type: IndicateurType) => {
  return VIEWS.filter((view) =>
    view.types.some((typeConfig) => {
      return typeConfig === type;
    }),
  );
};
