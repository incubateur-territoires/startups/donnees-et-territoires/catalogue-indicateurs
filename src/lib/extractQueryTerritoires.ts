import { KEYWORD_SEPARATOR, COMMA_SEPARATOR } from "@/constants/separators";

export const extractQueryTerritoires = (query: string) => {
  if (!query) {
    return [];
  }

  return query
    .split(COMMA_SEPARATOR)
    .map((territoire) => territoire.split(KEYWORD_SEPARATOR));
};
