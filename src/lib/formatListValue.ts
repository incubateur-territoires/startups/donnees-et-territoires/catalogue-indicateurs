export const formatListValue = ({
  value,
}: {
  value?: string[] | string | null;
}) => {
  if (value === null || value === undefined) {
    return "–";
  }

  if (typeof value === "string") {
    return value;
  }

  if (Array.isArray(value)) {
    return value.join(", ");
  }
};
