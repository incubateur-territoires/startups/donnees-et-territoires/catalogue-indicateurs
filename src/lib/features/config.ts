export const FEATURES_CONFIG = {
  auth: {
    enabled: true,
    decision: () => {
      return Boolean(process.env.NEXT_PUBLIC_AUTH_ENABLED);
    },
  },
};
