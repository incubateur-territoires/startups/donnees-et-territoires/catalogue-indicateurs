export const generateTableMarkup = <T extends { title: string }>(
  columns: T[],
  lines: (string | number)[][],
  label?: string,
) => {
  const cellFormat =
    'width=500 style="text-align: left; border: 1px solid #ddd;"';

  const header = `<thead><tr>${columns
    .map((column) => `<th ${cellFormat}>${column.title}</th>`)
    .join("")}</tr></thead>`;

  const body = `<tbody>${lines
    .map(
      (values) =>
        `<tr>${values
          .map((value) => {
            return `<td ${cellFormat}>${value}</td>`;
          })
          .join("")}</tr>`,
    )
    .join("")}</tbody>`;

  return `${label ? `${label}<br><br>` : ""}<table>${header}${body}</table>`;
};
