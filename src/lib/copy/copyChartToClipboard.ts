import { Chart } from "chart.js";

import { IMAGE_MIME_TYPE } from "@/constants/image";

import { getImageBlobFromUrl } from "./getImageBlobFromUrl";
import { getClipboard } from "./writeToClipboard";
import { isSafari } from "../isSafari";

export const copyChartToClipboard = async (
  ref: React.RefObject<Chart<"bar" | "pie", (string | number)[]>>,
) => {
  const src = ref.current?.canvas.toDataURL(IMAGE_MIME_TYPE) ?? "";

  // Safari requires using a promise resolving to a blob
  // instead of directly a blob to copy custom mime types
  const blob = isSafari()
    ? getImageBlobFromUrl(src)
    : await getImageBlobFromUrl(src);

  const { Item, clipboard } = getClipboard();

  await clipboard.write([
    new Item({
      [IMAGE_MIME_TYPE]: blob,
    }) as ClipboardItem,
  ]);
};
