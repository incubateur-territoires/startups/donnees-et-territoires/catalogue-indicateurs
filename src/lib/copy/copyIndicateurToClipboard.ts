import { UseQueryResult } from "@tanstack/react-query";

import { HIDDEN_METRICS } from "@/constants/indicateurs";
import { DIMENSIONS_TABLE_HEADERS, TABLE_HEADERS } from "@/constants/table";
import { DefinitionIndicateur, Metadata } from "@/types/indicateur";
import { Dimension } from "@/types/schema";
import { TerritoiresOptions } from "@/types/select";

import { generateListClipboardContent } from "./generateListClipboardContent";
import { generateTableMarkup } from "./generateTableMarkup";
import { generateTableText } from "./generateTableText";
import { writeToClipboard } from "./writeToClipboard";
import { NetworkError } from "../api/NetworkError";
import { formatNumericValue } from "../formatNumericValue";
import {
  extractIndicateurViewData,
  IndicateurViewData,
} from "../indicateurs/extractIndicateurViewData";
import { fillMissingCategories } from "../indicateurs/fillMissingCategories";
import { getCategories } from "../indicateurs/getCategories";
import { mapIndicateurData } from "../indicateurs/mapIndicateurData";
import { extractTitledHeaders } from "../table/extractTitledHeaders";

export const generateClipboardMarkup = (
  clipboardData: {
    data: IndicateurViewData[];
    definition: Metadata;
  }[],
) => {
  const tables = clipboardData.map((metric) =>
    generateTableMarkup(
      TABLE_HEADERS.map((header) => ({ title: header })),
      metric.data.map(({ label, count }) => [
        label,
        formatNumericValue({
          value: count,
          unit: metric.definition.unite,
        }),
      ]) as (string | number)[][],
      metric.definition.nom || "",
    ),
  );

  return tables.join("<br><br>");
};

export const generateClipboardText = (
  clipboardData: {
    data: IndicateurViewData[];
    definition: Metadata;
  }[],
) => {
  const tables = clipboardData.map((metric) =>
    generateTableText(
      TABLE_HEADERS.map((header) => ({ title: header })),
      metric.data.map(({ label, count }) => [label, count]) as (
        | string
        | number
      )[][],
      metric.definition.nom || "",
    ),
  );

  return tables.join("\n\n");
};

export const copyListToClipboard = async (
  data: string[] | Record<string, unknown>[],
  definition: Metadata,
) => {
  const label = definition.nom ?? "";
  let html: string;
  let text: string;

  if (typeof data[0] === "string") {
    ({ html, text } = generateListClipboardContent(data as string[], label));
  } else {
    const columns = extractTitledHeaders(definition.schema);
    const lines = (data as Record<string, string | number>[]).map((entry) =>
      columns.map((column) => entry[column.key] || ""),
    );

    html = generateTableMarkup(columns, lines, label);
    text = generateTableText(columns, lines, label);
  }

  await writeToClipboard(html, text);
};

export const copyIndicateurToClipboard = async (
  data: IndicateurViewData[],
  definition: Metadata,
) => {
  const definitionCategories = getCategories(
    definition.dimensions as Dimension[],
  );

  const headers = [
    ...(definitionCategories ? DIMENSIONS_TABLE_HEADERS : TABLE_HEADERS),
    ...(definitionCategories
      ? definitionCategories.map((category) => category.titre)
      : []),
  ].map((header) => ({ title: header }));
  const tableData = data.map(({ label, count, dimensions }) => {
    const categories = fillMissingCategories(
      getCategories(dimensions),
      definitionCategories,
    );

    return [
      label,
      formatNumericValue({ value: count, unit: definition.unite }),
      ...(categories ? categories.map((category) => category.valeur) : []),
    ];
  }) as (string | number)[][];

  const html = generateTableMarkup(headers, tableData, definition.nom || "");
  const text = generateTableText(headers, tableData, definition.nom || "");

  await writeToClipboard(html, text);
};

export const copyIndicateursToClipboard = async (
  definitions: Metadata[],
  data: UseQueryResult<DefinitionIndicateur[], NetworkError>[],
  territoires?: TerritoiresOptions,
) => {
  const clipboardData = definitions
    .filter((definition) => !HIDDEN_METRICS.includes(definition.identifiant))
    .map((definition) => {
      const mappedData = mapIndicateurData(definition.identifiant, data);

      const indicateurData = mappedData.map((entry) =>
        extractIndicateurViewData(entry.data, entry.isLoading, territoires),
      );

      return { data: indicateurData, definition };
    });

  const html = generateClipboardMarkup(clipboardData);
  const text = generateClipboardText(clipboardData);

  await writeToClipboard(html, text);
};
