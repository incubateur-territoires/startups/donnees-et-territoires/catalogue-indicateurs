import { PAYS_OPTION } from "@/constants/mailles";
import { TypeIndicateur } from "@/types/schema";
import { TerritoireOption, TerritoiresOptions } from "@/types/select";

export const getMatchingOptions = (
  indicateur: TypeIndicateur,
  options: TerritoiresOptions | undefined,
) => {
  if (!options) {
    return [PAYS_OPTION];
  }

  if (indicateur.maille === "département") {
    return options.reduce<TerritoireOption[]>(
      (departements, region) =>
        "options" in region
          ? [...departements, ...region.options]
          : departements,
      [],
    );
  }

  if (indicateur.maille === "région") {
    return options;
  }

  return [PAYS_OPTION];
};
