import { Root } from "mdast";
import { Parent } from "unist";
import { visit } from "unist-util-visit";

const skipLinks = (node: Parent, index: number, parent: Parent): void => {
  parent.children = [
    ...parent.children.slice(0, index),
    node.children[0],
    ...parent.children.slice(index + 1),
  ];
};

export function remarkSkipLinks() {
  return (tree: Root) => {
    // unist-util-visit type are undecipherable, so we'll use any here
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    visit(tree, (node) => node.type === "link", skipLinks as any);
  };
}
