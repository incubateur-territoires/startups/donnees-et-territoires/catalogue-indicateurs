import { Root } from "mdast";
import { Parent } from "unist";
import { visit } from "unist-util-visit";

const extendLinks = (node: Parent, index: number, parent: Parent): void => {
  parent.children = [
    ...parent.children.slice(0, index),
    {
      ...node,
      data: {
        hProperties: {
          className: "fr-link",
          target: "_blank",
          rel: "noreferrer",
        },
      },
    },
    ...parent.children.slice(index + 1),
  ];
};

export function remarkDsfrLinks() {
  return (tree: Root) => {
    // unist-util-visit types are undecipherable, so we'll use any here
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    visit(tree, (node) => node.type === "link", extendLinks as any);
  };
}
