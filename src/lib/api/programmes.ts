import { CATEGORIES } from "@/constants/categories";
import { PROGRAMMES } from "@/constants/programmes";
import { Metadata } from "@/types/indicateur";
import { Programme } from "@/types/programme";
import { OptionWithCount } from "@/types/select";

export function fetchProgrammes() {
  return PROGRAMMES;
}

function sortByCategorie(programmes: Programme[], definitions: Metadata[]) {
  return programmes.reduce<{ [key: string]: OptionWithCount[] }>(
    (acc, programme) => {
      const count = definitions.filter((definition) =>
        definition.tags.some(
          (tag) =>
            tag.name === "programme" && tag.value === programme.identifiant,
        ),
      ).length;

      if (count === 0) {
        return acc;
      }

      const category = CATEGORIES.find(
        (candidate) => candidate.identifiant === programme.categorie,
      ) || { identifiant: "autres" as const };

      if (!(category.identifiant in acc)) {
        acc[category.identifiant] = [];
      }

      return {
        ...acc,
        [category.identifiant]: [
          ...acc[category.identifiant],
          { value: programme.identifiant, label: programme.nom, count },
        ],
      };
    },
    {},
  );
}

export type ProgrammeOptions = {
  label: string;
  value: string;
  options: OptionWithCount[];
  count: number;
};

export function getProgrammesOptions(
  programmes: Programme[],
  definitions: Metadata[],
): ProgrammeOptions[] {
  const programmesByCategorie = sortByCategorie(programmes, definitions);
  return CATEGORIES.map((category) => ({
    label: category.nom,
    value: category.nom,
    options: programmesByCategorie[category.identifiant],
    count: programmesByCategorie[category.identifiant].reduce(
      (acc, programmes) => acc + programmes.count,
      0,
    ),
  }));
}
